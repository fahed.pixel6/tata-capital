﻿import { Directive, Inject, HostListener, ElementRef } from '@angular/core'
import { NgControl } from '@angular/forms';

import { APP_CONFIG } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';

/* 	numberOnly alphaOnly alphaWithSpaces alphaNumericOnly alphaNumericWithSpaceOnly alphaNumericWithSpecialsOnly 
	alphaNumericWithSpecialsAndSpaceOnly numberWithSlashOnly date limitLength disableCopy */

@Directive({
	selector: '[numberOnly]',
	inputs: ['length', 'cropZero'],
})
export class NumberOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private cropZero: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.cropZero = true;
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");

		newValue = newValue.replace(re, '');
		
		if (this.cropZero) {
			newValue = initalValue.replace(/^0(0+)?/g, '0');
		}
		newValue = newValue.replace(/[^0-9]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}

	/* Used for OTP fields with dashed Input for backspace */
	@HostListener('keyup', ['$event']) onKeyPressUp(event) {
        if (this.el.nativeElement.value.length >= this.length) {
            if (event.srcElement.nextElementSibling) {
                event.srcElement.nextElementSibling.focus();
            }
        }
        if (event.keyCode == 8 && event.srcElement.previousElementSibling) {
            event.srcElement.previousElementSibling.focus();
        }
    }
}

@Directive({
	selector: '[alphaOnly]',
	inputs: ['length', 'upperOnly'],
})
export class AlphaOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedAllSpecialCharsRegEx, "g");

		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^a-zA-Z]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[alphaWithSpaces]',
	inputs: ['length'],
})
export class AlphaWithSpacesDirective {
	appConfig: IAppConfig;
	private length: number;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedAllSpecialCharsRegEx, "g");

		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/^\s/g, '');
		newValue = newValue.replace(/[^a-zA-Z\s]*/g, '');
		newValue = newValue.replace(/\s\s/g, ' ');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[alphaNumericOnly]',
	inputs: ['length', 'upperOnly']
})
export class AlphaNumericOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");

		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9a-zA-Z]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

// Used for Full Name
@Directive({
	selector: '[alphaNumericWithSpaceOnly]',
	inputs: ['length', 'upperOnly']
})
export class AlphaNumericWithSpaceOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, 'g');

		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/^\s/g, '');
		newValue = newValue.replace(/[^0-9a-zA-Z\s]*/g, '');
		newValue = newValue.replace(/\s\s/g, ' ');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length);
		}

		if (this.upperOnly) { newValue = newValue.toUpperCase(); }
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

// Used for Email
@Directive({
	selector: '[alphaNumericWithSpecialsOnly]',
	inputs: ['length', 'upperOnly']
})
export class AlphaNumericWithSpecialsOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");

		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9a-zA-Z-._@]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

// Used for Textarea , Address
@Directive({
	selector: '[alphaNumericWithSpecialsAndSpaceOnly]',
	inputs: ['length', 'upperOnly']
})
export class AlphaNumericWithSpecialsAndSpaceOnlyDirective {
	appConfig: IAppConfig;

	private length: number;
	private upperOnly: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");

		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/^\s/g, '');
		newValue = newValue.replace(/[^0-9a-zA-Z.,;:[](){}\/\\'\-\s]*/g, '');
		newValue = newValue.replace(/\s\s/g, ' ');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		if (this.upperOnly) newValue = newValue.toUpperCase();
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
	selector: '[numberWithSlashOnly]',
	inputs: ['length'],
})
export class NumberWithSlashOnlyDirective {
	appConfig: IAppConfig;

	private length: number;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}

	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");

		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/[^0-9.\/]*/g, '');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}
}

@Directive({
		selector: '[date]',
		inputs: ['length'],
	})
	export class DateFormatterDirective {
	appConfig: IAppConfig;

	private length: number;
  
	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}
   
	@HostListener('input', ['$event']) onInputChange(event) {
		const initalValue = this.el.nativeElement.value;
		let newValue = initalValue;
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");

		newValue = newValue.replace(re, '');
		newValue = newValue.replace(/^\//, '');
		newValue = newValue.replace(/[^0-9\/]*/g, '');
		newValue = newValue.replace(/\/\//g, '\/');

		if (this.length && newValue.length > this.length) {
			newValue = newValue.substring(0, this.length)
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);

		if (initalValue !== this.el.nativeElement.value) {
			event.stopPropagation();
		}
	}

	@HostListener('keyup', ['$event']) onKeyPress(event) {
		const initalValue = this.el.nativeElement.value;
		let valueLength = initalValue.length;
		let newValue = initalValue;

		if (event.keyCode != 8 && (valueLength === 2 || valueLength === 5)) {
			newValue += '/';
		}
		this.el.nativeElement.value = newValue;
		this.control.control.setValue(newValue);
	}
}

@Directive({
	selector: '[limitLength]',
	inputs: ['length'],
})
export class LimitLengthDirective {
	appConfig: IAppConfig;
	private length: number;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
				private el: ElementRef,
				private control: NgControl) {
		this.appConfig = appConfig;
	}

	@HostListener('blur', ['$event.target.value']) onBlur(value) {
		var re = new RegExp(this.appConfig.restrictedCharsRegEx, "g");
		value = value.replace(re, '');
		this.control.control.setValue(value);
	}

	@HostListener('keydown', ['$event']) onKeyPress(event) {
		let e = <KeyboardEvent>event;
		if (e.shiftKey && this.appConfig.restrictedChars.indexOf(e.keyCode) !== -1) {
			e.preventDefault();
		}
		if (this.length && this.el.nativeElement.value.length >= this.length) {
			e.preventDefault();
		}
	}
}

@Directive({
	selector: '[disableCopy]',
	inputs: ['length']
})
export class DisableCopyDirective {
	@HostListener('keydown', ['$event']) onKeyPress(event) {
		let e = <KeyboardEvent>event;
		if ((e.keyCode === 65 && (e.ctrlKey || e.metaKey)) ||
		// Allow: Ctrl+C
		(e.keyCode === 67 && (e.ctrlKey || e.metaKey)) ||
		// Allow: Ctrl+V
		(e.keyCode === 86 && (e.ctrlKey || e.metaKey)) ||
		// Allow: Ctrl+X
		(e.keyCode === 88 && (e.ctrlKey || e.metaKey))) {
			e.preventDefault();
		}
	}
}
