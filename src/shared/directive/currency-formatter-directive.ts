﻿import { Directive, HostListener, ElementRef, OnInit } from '@angular/core';
import { NgControl } from "@angular/forms";

import { IndianCurrencyPipe, floatPipe } from '../pipe/currency-pipe';

@Directive({ selector: '[indianCurrencyFormatter]', inputs: ['fraction', 'length', 'negative'] })
export class IndianCurrencyFormatterDirective implements OnInit {

    private el: HTMLInputElement;
    private fraction: number;
    private length: number;
    private negative: boolean;
    private totalLength: number;
    private valueSubscription: any;

    constructor(private elementRef: ElementRef,
        private control: NgControl,
        private currencyPipe: IndianCurrencyPipe) {

        this.el = this.elementRef.nativeElement;
    }

    ngOnInit() {

        this.fraction = this.fraction ? this.fraction : 2;
        this.length = this.length ? this.length : 14;
        this.negative = this.negative ? this.negative : false;

        if (this.fraction > 0) {
            this.totalLength = Number(this.length) + Number(this.fraction) + Number(1);
        } else {
            this.totalLength = Number(this.length);
        }

        if (this.negative) this.totalLength += Number(1);

        this.valueSubscription = this.control.control.valueChanges.subscribe((value: string) => {
            if (value) {
                let formatted = this.currencyPipe.transform(value, this.fraction, this.negative);
                this.el.value = formatted;
            }
            this.valueSubscription.unsubscribe();
        });
    }
/*
    @HostListener('focus', ['$event.target.value']) onFocus(value) {

        if (!this.valueSubscription.closed) {
            this.valueSubscription.unsubscribe();
        }
        let val = this.currencyPipe.parse(value, this.fraction); // opossite of transform
        this.control.control.setValue(val);
    }

    @HostListener('blur', ['$event.target.value']) onBlur(value) {

        let formatted = this.currencyPipe.transform(value, this.fraction, this.negative);
        let parsed = this.currencyPipe.parse(formatted, this.fraction);

        this.el.value = formatted;
        this.control.control.setValue(parsed, {
            emitEvent: false,
            emitModelToViewChange: false,
            emitViewToModelChange: false
        });
    }
*/
    @HostListener('input', ['$event']) onInputChange(event) {
        const initalValue = this.el.value;
        let newValue = initalValue;
        let shiftCursorBy = 0;

        if (this.fraction > 0) {
            newValue = newValue.replace(/[^0-9.]*/g, '');
        }else{
            newValue = newValue.replace(/[^0-9]*/g, '');
        }
        
        if (this.length && newValue.length > this.length) {
            newValue = newValue.substring(0, this.length)
        }
        let start = this.el.selectionStart;
        let end = this.el.selectionEnd;

        newValue = this.currencyPipe.transform(newValue, this.fraction);
        let parsed = this.currencyPipe.parse(newValue, this.fraction);
        
        if(initalValue.length < newValue.length){
            shiftCursorBy = 1;
        }else if(initalValue.length > newValue.length){
            shiftCursorBy = -1;
        }
        this.control.valueAccessor.writeValue(newValue);
        this.el.setSelectionRange(start+shiftCursorBy, end+shiftCursorBy);

        this.control.control.setValue(parsed, {
            emitEvent: false,
            emitModelToViewChange: false,
            emitViewToModelChange: false
        });

        if (initalValue !== this.el.value) {
            event.stopPropagation();
        }
    }
}


@Directive({ selector: '[floatFormatter]', inputs: ['fraction', 'length'] })
export class FloatFormatterDirective implements OnInit {

    private el: HTMLInputElement;
    private fraction: number;
    private length: number;
    private totalLength: number;

    constructor(private elementRef: ElementRef,
        private control: NgControl,
        private floatPipe: floatPipe) {

        this.el = this.elementRef.nativeElement;
    }

    ngOnInit() {
        this.fraction = this.fraction ? this.fraction : 2;
        this.length = this.length ? this.length : 14;

        this.totalLength = Number(this.length) + Number(this.fraction) + Number(1);

        const handle = this.control.control.valueChanges.subscribe((value: string) => {
            if (value) {
                let formatted = this.floatPipe.transform(value, this.fraction);
                this.el.value = formatted;
                handle.unsubscribe();
            }
        });
    }
/*
    @HostListener('focus', ['$event.target.value']) onFocus(value) {
        let val = this.floatPipe.parse(value, this.fraction); // opossite of transform
        this.control.control.setValue(val);
    }

    @HostListener('blur', ['$event.target.value']) onBlur(value) {

        let formatted = this.floatPipe.transform(value, this.fraction);
        let parsed = this.floatPipe.parse(formatted, this.fraction);

        this.el.value = formatted;
        this.control.control.setValue(parsed, {
            emitEvent: false,
            emitModelToViewChange: false,
            emitViewToModelChange: false
        });
    }
*/
    

    @HostListener('keyup', ['$event']) onInputChange(event) {
        const initalValue = this.el.value;
        let newValue = initalValue;

        if (this.fraction > 0) {
            newValue = newValue.replace(/[^0-9.]*/g, '');
        }else{
            newValue = newValue.replace(/[^0-9]*/g, '');
        }

        if (this.length && newValue.length > this.length) {
            newValue = newValue.substring(0, this.length)
        }
        let start = this.el.selectionStart;
        let end = this.el.selectionEnd;

        this.control.valueAccessor.writeValue(newValue);
        this.el.setSelectionRange(start, end);
        this.control.control.setValue(newValue);


        if (initalValue !== this.el.value) {
            event.stopPropagation();
        }
    }
}