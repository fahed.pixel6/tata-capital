import { Component, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

import { Alert, AlertType } from '../models/alert';

@Component({
    moduleId: module.id,
    template: `<mat-dialog-content>
                    <div class="alert-wrapper">
                        <a class="close close-icon" (click)="closePopup()"><i class="material-icons md-24 md-dark" >cancel</i></a>
                        <div class="alert {{ cssClass(alert) }} " [innerHtml]="alert.message"></div>
                    </div>
                </mat-dialog-content>`,
    styles: ['']        
})

export class AlertComponent {
    alert: Alert;

    constructor(public dialogRef: MatDialogRef<AlertComponent>,
                @Inject(MAT_DIALOG_DATA) public data: any) { 
        this.alert = data;
    }

    ngOnInit() {
    }

    closePopup() {
        this.dialogRef.close();
    }

    cssClass(alert: Alert) {
        if (!alert) {
            return;
        }
        // return css class based on alert type
        switch (alert.type) {
            case AlertType.Success:
                return 'alert alert-success';
            case AlertType.Error:
                return 'alert alert-danger';
            case AlertType.Info:
                return 'alert alert-info';
            case AlertType.Warning:
                return 'alert alert-warning';
        }
    }
}
