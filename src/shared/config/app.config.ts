import { InjectionToken } from '@angular/core';

import { environment } from '../../environments/environment';

import { IAppConfig } from './iapp.config';
import { AppCustomerConfig } from './customer.config';

export let APP_CONFIG = new InjectionToken('app.config');

    let envConfig: IAppConfig;

    envConfig = AppCustomerConfig;

    if (environment.delegateUrl) {
        envConfig.host = environment.delegateUrl;
    }
    if (environment.onlineUrl) {
        envConfig.hostCallback = environment.onlineUrl;
    }
/*
envConfig.restrictedChars = [49, 52, 54, 56, 188, 190];
envConfig.restrictedCharsRegEx = '[!\\$\\^*<>]';
envConfig.restrictedAllSpecialCharsRegEx = '[!\\$\\^\'*<>_@#$%&()-=+{};:,./?"|~`]';
*/
export const AppConfig: IAppConfig = envConfig;
