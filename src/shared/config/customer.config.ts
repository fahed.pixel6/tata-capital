import { IAppConfig } from './iapp.config';

export const AppCustomerConfig: IAppConfig = {
    host: 'http://45.114.77.214:8082/CLIPServices/',
    hostCallback: 'http://45.114.77.214:8308/',

    restrictedChars: [],
    restrictedCharsRegEx: '[]',
    restrictedAllSpecialCharsRegEx: '[]',
    
    endpoints: {
    
        perfiosRedirect: 'online_bl/perfios/services/getPerfios',
        getOtp: 'otp/services/generateOtp',
        getEmailOtp: 'authentication/mobile/v0.1/generateOtpForEmail',
        verifyOtp: 'otp/services/verifyOtp',
        verifyEmailOtp: 'authentication/mobile/v0.1/verifyotpForEmail',
        refreshToken: 'token/services/generate-token',

        getDetailsByPan: 'pan/services/getPanDetails',
        getCityNames: 'city/getCityNames',

        getMembershipCAValidate: 'sme/registrationValidate/getMembershipCAValidate',
        getMembershipCOAValidate: 'sme/registrationValidate/getMembershipCOAValidate',
        getDoctorVerificationService: 'sme/registrationValidate/getDoctorVerificationService',

        getITRCaptcha: 'sme/ITRService/getITRCaptcha',
        getItrData: 'sme/ITRService/getITRData',

        getBasicDetails: 'sme/businessdetails/getBasicDetails',
        saveBasicDetails: 'sme/businessdetails/saveBasicDetails',

        getBusinessDetails: 'sme/businessdetails/getBusinessDetails',
        saveBusinessDetails: 'sme/businessdetails/saveBusinessDetails',

        saveFinancialDetails:  'sme/financialdetails/saveFinancialDetails',
        getFinancialDetails:  'sme/financialdetails/getFinancialDetails',

        getCustomerProgramDetails: 'sme/ProgramType/getCustomerProgramDetails',
        saveProgramDetails:  'sme/ProgramDetailsSaveService/saveProgramDetails',
        getProgramTypeDetails: 'sme/ProgramType/getProgramTypeDetails',

        getApplicationDetails: 'sme/appform/getAppFormDetails',
        saveApplicationDetails: 'sme/appform/saveAppFormDetails',

        getLoanDetails: 'sme/loanservice/getLoanDetails',
        getEMIAmount: 'sme/EMIService/getEMIAmount',
        saveApprovalDetails:  'sme/loanservice/saveLoanDetails',

        getMcaCaptcha: 'sme/mca/getMcaCaptcha',
        getMcaInfo: 'sme/mca/getMcaInfo',

        getNeedMore: 'NeedMoreService/needMore',

        gstGenOtp: 'sme/GSTService/gstGenOtp',
        gstSubOtp: 'sme/GSTService/gstSubOtp',

        uploadDoc: 'documentupload/services/uploadDoc',
        getDocumentDetails: 'documentupload/services/getUploadedDocs',
        
        getPLRate: 'plrate/services/v0.1/getPLRate',
        getFinalizeCalculation: 'emi/services/v0.1/getEMIAmount',
        saveFinalizeDetails: 'loandetails/services/v0.1/handleLoanDetails',
        savePerfiosStatus: 'perfios/services/v0.1/savePerfiosStatus',

        sendSMS: 'customercare/email/sendEmail',
        sendDsaSMS: 'sms/services/sendsms',
        getConsent: 'consent/getConsent'

    },
    lookups: {
        lookupSegmentDetails: 'sme/lookup/getSegementDetails',
        lookupIndustryType: 'sme/lookup/getIndustryTypes',
        lookupOwnershipCheck: 'sme/lookup/getOwnershipCheck',
        lookupYearsOfBusiness: 'sme/lookup/getYearsOfBusiness',
        lookupLoanTenure: 'sme/lookup/getTenures',
        lookupGender: 'sme/lookup/getGenderLookup',
        getMaritalStatus: 'sme/lookup/getMaritalStatusLookup',
        getUniversityLookup: 'sme/colleges/getColleges',
        lookupStateMedicalCouncil: 'sme/lookup/getMedicalCouncilLookup',
        documentListByCustomerId: 'documentupload/services/documentListByCustomerId',
        getYearsInCurrentAddress: 'sme/lookup/getYearsInCurrAddrLookup',
        getYearsInCurrentCity: 'sme/lookup/getYearsInCurrCityLookup',
        getAddressType: 'sme/lookup/getAddressType',
        getAccommodationType: 'sme/lookup/getAccommodationType',
    },
    customerlookups:{
    }
}
