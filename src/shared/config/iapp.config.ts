export interface IAppConfig {
	host: string;
	hostCallback: string;
	endpoints: any;
	lookups: any;
	customerlookups: any;
	restrictedChars: any;
	restrictedCharsRegEx: any;
	restrictedAllSpecialCharsRegEx: any;
}
