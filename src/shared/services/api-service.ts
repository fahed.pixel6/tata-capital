
import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { throwError as observableThrowError, Observable, Subject, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import * as moment from 'moment';

import { APP_CONFIG, AppConfig } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';

import { Cache } from './cache';

@Injectable()
export class ApiService {

    appConfig: IAppConfig;

    private actionSource = new Subject<any>();
    currentAction = this.actionSource.asObservable();
    commonStrings: any;

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
                private http: HttpClient,
                private cache: Cache) {

        this.appConfig = appConfig;
        this.commonStrings = {
            http_error: "Something gone wrong...",
            download_error: "Unable to download file...",
            data_saved: "Data is saved",
            reject_captcha: "Captcha validation failed",
            file_size: "Please select Image to upload smaller than 5Mb and greater than 25Kb",
            file_extention: "Please select the file format as jpeg, jpg, pdf and png.",
            thanks_msg: "Thankyou for choosing Tata Capital. Our customer representative will get in touch with you shortly.", 
        }
    }
    
    getCurrentDate(days) {
        let d = null;
        if (days < 0) {
            d = moment().subtract(1, 'days');
        } else {
            d = moment().add(days, 'days');
        }
        return {
            year: Number(d.format('YYYY')),
            month: Number(d.format('M')),
            day: Number(d.format('D')),
        };
    }

    sendAction(message: any) {
        this.actionSource.next(message)
    }

    isDropOff(resp) {
        if (resp && resp.status && resp.status.statusCode != '200' && 
                                    resp.status.statusCode != '704' && 
                                    resp.status.statusCode != '706' && 
                                    resp.status.statusCode != '715' && 
                                    resp.status.statusCode != '719' && 
                                    resp.status.statusCode != '727' && 
                                    resp.status.statusCode != '734' &&  
                                    resp.status.statusCode != '999') {
            return true;
        }
        return false;
    }

    isDropOffSuccess(statusCode) {
        if (statusCode == '200' || statusCode == '756' || statusCode == '754' || statusCode == '753' || statusCode == '711' || statusCode == '726') {
            return true;
        }
        return false;
    }

    transformDate(date : any) {
        if (date) {
            if (date.formatted) {
                return date.formatted;
            } else if(date.date) {
                return date.date.day + '/' + date.date.month + '/' + date.date.year;
            } else if(date) {
                return date.day + '/' + date.month + '/' + date.year;
            } else {
                return '';
            }
        } else {
            return '';
        }
    }

    convertToInteger(value) {
        return parseInt(value);
    }

    appendCommonParameters(data) {
        if (data == undefined || !data)  data = {};

        if (this.cache.user.isAdmin) {
            data.source = "MISTCLTB";
            if (data.loginId == undefined)  data.loginId = this.cache.user && this.cache.user.customerId ? this.cache.user.customerId: 0;

            if (data.loginId == 0) delete data.loginId;
        } else {
            if (data.customerId == undefined)  data.customerId = this.cache.user && this.cache.user.customerId ? this.cache.user.customerId: 0;
            if (data.customerId == 0) delete data.customerId;
        }
        return data;
    }

    /*Commomn function to POST FILE form data */
    upload(url, data) {
        return new Promise((resolve, reject) => {
            this.http.post(this.appConfig.host+url, data)
                .pipe(map(res => res as {}), catchError(this.handleError([])))
                .subscribe((res: any) => {
                    if (res && res.uploadStatus == 'Success') {
                        resolve(res);
                    } else {
                        reject((res && res.errorDesc) ? res.errorDesc : this.commonStrings.http_error);
                    }
                }, (error) => {
                    reject(this.commonStrings.http_error);
                });
        });
    }

    getApi(url, params, headers) {

        if (!headers || !headers['Content-Type']) {
            headers = new HttpHeaders({ "Content-Type": "application/json" });
        }
        params = this.appendCommonParameters(params);

        return new Promise((resolve, reject) => {
            this.http.get(this.appConfig.host+url, {params: params, headers: headers})
                .pipe(map(res => res as {}), catchError(this.handleError([])))
                .subscribe(res => {
                    resolve(res);
                }, (error) => {
                    reject(this.commonStrings.http_error);
                });
        });
    }

    postApi(url, data, headers) {

        if (!headers || !headers.get('Content-Type')) {
            headers = new HttpHeaders({ "Content-Type": "application/json" });
        }
        data = this.appendCommonParameters(data);
        
        return new Promise((resolve, reject) => {
            this.http.post(this.appConfig.host+url, JSON.stringify(data), {headers: headers})
                .pipe(map(res => res as {}), catchError(this.handleError([])))
                .subscribe(res => {
                    resolve(res);
                }, (error) => {
                    reject(this.commonStrings.http_error);
                });
        });
    }

    postApiCancellable(url, data, headers) {
        if (!headers || !headers.get('Content-Type')) {
            headers = new HttpHeaders({ "Content-Type": "application/json" });
        }
        data = this.appendCommonParameters(data);

        return this.http.post(this.appConfig.host+url, JSON.stringify(data), {headers: headers})
                .pipe(map(res => res as {}), catchError(this.handleError([])));           
    }
    
    private handleError<T> (result?: T) {
        return (error: any): Observable<T> => {

          // TODO: send the error to remote logging infrastructure
          console.error(error); // log to console instead

          // Let the app keep running by returning an empty result.
          return of(error.error);
        };
    }
}
