import { Injectable, Inject } from '@angular/core';

import { APP_CONFIG, AppConfig } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';
import { ApiService } from '../services/api-service';

@Injectable()
export class ConstantsService {

    appConfig: IAppConfig;
    constants: any = [];

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
                private apiService: ApiService) {

        this.appConfig = appConfig;

        this.constants = {
            industryType: [],
            resiOfficeOwnership: [],
            yearsInBusiness: [],
            segmentOfBusiness: [],
            loanTenure: [],
            universityName: [],
            gender: [],
            stateMedicalCouncil: [],
            customerDocumentList: [],
            groupDocumentList: [],
            professionType: [],
            marritalStatus: [],
            yearsInCurrentAddress: [],
            yearsInCurrentCity: [],
            addressType: [],
            accommodationType: [],
        }
    }

    getsegmentOfBusiness() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupSegmentDetails, '','').then((resp: any) => {
                if (resp && resp.jcmsLookupVo) {
                    this.constants.segmentOfBusiness = resp.jcmsLookupVo;
                }
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getloanTenure() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupLoanTenure, '','').then((resp: any) => {
                if (resp && resp.jcmsLookupVo) {
                    this.constants.loanTenure = resp.jcmsLookupVo;
                }
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getGender() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupGender, '','').then((resp: any) => {
                if (resp && resp.jcmsLookupVo) {
                    this.constants.gender = resp.jcmsLookupVo;
                }
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getIndustryType() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupIndustryType, '','').then((resp: any) => {
                if (resp && resp.industryLookuplist) {
                    this.constants.industryType = resp.industryLookuplist;
                }
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getOwnershipCheck() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupOwnershipCheck, '','').then((resp: any) => {
                if (resp && resp.jcmsLookupVo) {
                    this.constants.resiOfficeOwnership = resp.jcmsLookupVo;
                }
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getYearsOfBusiness() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupYearsOfBusiness, '','').then((resp: any) => {
                if (resp && resp.jcmsLookupVo) {
                    this.constants.yearsInBusiness = resp.jcmsLookupVo;
                }
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getUniversityName() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.getUniversityLookup, '','').then((resp: any) => {
                if (resp && resp.collegesNames) {
                    this.constants.universityName = resp.collegesNames;
                }
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getStateMedicalCouncil() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupStateMedicalCouncil, '','').then((resp: any) => {
                this.constants.stateMedicalCouncil = resp.medicalCouncilLookupList;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getAllDocumentList() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.documentListByCustomerId, '','').then((resp: any) => {
                this.constants.customerDocumentList = resp;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getMarritalStatus() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.getMaritalStatus, '','').then((resp: any) => {
                this.constants.maritalStatus = resp.jcmsLookupVo;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getYearsInCurrentAddress() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.getYearsInCurrentAddress, '','').then((resp: any) => {
                this.constants.yearsInCurrentAddress = resp.jcmsLookupVo;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getYearsInCurrentCity() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.getYearsInCurrentCity, '','').then((resp: any) => {
                this.constants.yearsInCurrentCity = resp.jcmsLookupVo;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getAddressType() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.getAddressType, '','').then((resp: any) => {
                this.constants.addressType = resp.jcmsLookupVo;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

    getAccommodationType() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.getAccommodationType, '','').then((resp: any) => {
                this.constants.accommodationType = resp.jcmsLookupVo;
                resolve(true);
            }, (error) => {
                reject(error);
            });
        });
    }

/*
    getLookupByName(lookup) {
        let data = { "lookupName": lookup, "customerId": 0 };
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupByName, data).then((resp: any) => {
                if (resp) {

                }
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }
*/
}
