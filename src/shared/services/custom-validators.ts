import { Injectable } from '@angular/core';
import { FormControl, FormGroupDirective, NgForm } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material';

@Injectable()
export class CustomValidators {
    
    bankNameValidator(control: FormControl) {
        let bankname = control.value;
        if (!bankname) {
            return {
                bankname: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^[ A-Za-z0-9.,&-]*$/);
            if (!re.test(bankname)) {
                return {
                    bankname: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    compNameValidator(control: FormControl) {
        let compName = control.value;
        if (!compName) {
            return {
                compName: {
                    required: true
                }
            }
        } else if(compName.length > 255) {
            return {
                compName: {
                    maxlength: true
                }
            }
        } else {
            var re = new RegExp(/^[ A-Za-z0-9.,&\-()]*$/);
            if (!re.test(compName)) {
                return {
                    compName: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    fullNameValidator(control: FormControl) {
        let fullName = control.value;
        if (!fullName) {
            return {
                fullName: {
                    required: true
                }
            }
        } else if(fullName.length > 100) {
            return {
                fullName: {
                    maxlength: true
                }
            }
        } else {
            var re = new RegExp(/^[A-Za-z]+$/);
            if (!re.test(fullName)) {
                return {
                    fullName: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    NameValidator(control: FormControl) {
        let fullName = control.value;
        if (!fullName) {
            return {
                fullName: {
                    required: true
                }
            }
        } else if (fullName.length > 100) {
            return {
                fullName: {
                    maxlength: true
                }
            }
        } else {
            var re = new RegExp(/^[ A-Za-z]+$/);
            if (!re.test(fullName)) {
                return {
                    fullName: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    // reference name validator
    refNameValidator(control: FormControl) {
        let refName = control.value;
        if (!refName) {
            return {
                refName: {
                    required: true
                }
            }
        } else if(refName.length > 100) {
            return {
                refName: {
                    maxlength: true
                }
            }
        } else {
            var re = new RegExp(/^([a-zA-Z]+[\.\s]?[a-zA-Z]*)*$/);
            if (!re.test(refName)) {
                return {
                    refName: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    optionalNameValidator(control: FormControl) {
        let fullName = control.value;
        if (fullName) {
            if (fullName.length > 100) {
                return {
                    fullName: {
                        maxlength: true
                    }
                }
            } else {
                var re = new RegExp(/^[ A-Za-z]*$/);
                if (!re.test(fullName)) {
                    return {
                        fullName: {
                            pattern: true
                        }
                    }
                }
            }
        }
        return null;
    }

    panValidator(control: FormControl) {
        let pan = control.value;
        if (!pan) {
            return {
                pan: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^[A-Z]{3}[CHFATBLJGP][A-Z][0-9]{4}[A-Z]$/);
            if (!re.test(pan)) {
                return {
                    pan: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    personPanValidator(control: FormControl) {
        let pan = control.value;
        if (!pan) {
            return {
                pan: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^[A-Z]{3}[P][A-Z][0-9]{4}[A-Z]$/);
            if (!re.test(pan)) {
                return {
                    pan: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    ifscValidator(control: FormControl) {
        let ifscCode = control.value;
        if (!ifscCode) {
            return {
                ifscCode: {
                    required: true
                }
            }
        } else {
            var re =  new RegExp(/^[A-Z]{4}[0]{1}[0-9]{6}$/);
            if (!re.test(ifscCode)) {
                return {
                    ifscCode: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    otpValidator(control: FormControl) {
        let otp = control.value;
        if (!otp) {
            return {
                otp: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^[0-9]{4,6}$/);
            if (!re.test(otp)) {
                return {
                    otp: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    aadharValidator(control: FormControl) {
        let aadhar = control.value;
        if (!aadhar) {
            return {
                aadhar: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^[A-Z0-9]{12}$/);
            if (!re.test(aadhar)) {
                return {
                    aadhar: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    addressValidator(control: FormControl) {
        let address = control.value;
        if (!address) {
            return {
                address: {
                    required: true
                }
            }
        } else if(address.length > 255) {
            return {
                address: {
                    maxlength: true
                }
            }
        } else {
            var re = new RegExp(/^[ A-Za-z0-9.,-/\(\)\[\]]*$/);
            if (!re.test(address)) {
                return {
                    address: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    cityValidator(control: FormControl) {
        let city = control.value;
        if (!city) {
            return {
                city: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^[ A-Za-z.,-]{0,255}$/);
            if (!re.test(city)) {
                return {
                    city: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    postalCodeValidator(control: FormControl) {
        let postal = control.value;
        if (!postal) {
            return {
                postalCode: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^[1-9][0-9]{5}$/);
            if (!re.test(postal)) {
                return {
                    postalCode: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    phoneRegExpValidator(control: FormControl) {
        if(control.value) {
            let phone = control.value;
            var re = new RegExp(/^((91))?[0-9]{10}$/);
            if (!re.test(phone)) {
                return {
                    phone: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    mobileValidator(control: FormControl) {
        let mobile = control.value ? control.value : 0;
        if (!mobile) {
            return {
                mobile: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^([6-9]{1})[0-9]{9}$/);
            if (!re.test(mobile)) {
                return {
                    mobile: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    extentionValidator(control: FormControl) {
        let extention = control.value;
        if (!extention) {
            return null;
        }
        if (extention.length > 4) {
            return {
                extention: {
                    maxlength: true
                }
            }
        } else {
            var re = new RegExp(/^[0-9]*$/);
            if (!re.test(extention)) {
                return {
                    extention: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    emailValidator(control: FormControl) {
        let email = control.value;
        if (!email) {
            return {
                email: {
                    required: true
                }
            }
        } else {
            var re = new RegExp(/^[A-Za-z]+[A-Za-z0-9._]+@[a-zA-Z0-9]+([.]{1}[a-zA-Z]{2,})+$/);
            if (email.length > 100) {
                return {
                    email: {
                        maxlength: true
                    }
                };
            } else if (!re.test(email)) {
                return {
                    email: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    optionalemailValidator(control: FormControl) {
        let email = control.value;
        if (email) {
            var re = new RegExp(/^[A-Za-z]+[A-Za-z0-9._]+@[a-zA-Z0-9]+([.]{1}[a-zA-Z]{2,})+$/);
            if (email.length > 100) {
                return {
                    email: {
                        maxlength: true
                    }
                };
              } else if (!re.test(email)) {
                return {
                    email: {
                        pattern: true
                    }
                }
            }
        }
        return null;
    }

    gstValidator(control: FormControl) {
        let gstIn = control.value;
            if (!gstIn) {
                return {
                    gstIn: {
                        required: true
                    }
                }
            } else {
                var re =  new RegExp(/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/);
                if (!re.test(gstIn)) {
                    return {
                        gstIn: {
                            pattern: true
                        }
                    }
                }
            }
            return null;
       
    }

    /*validation for admin app gstin*/
    optionalgstinValidator(control: FormControl) {
        const selector = control.value;
        const re = new RegExp(/^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$/);
        if (selector) {
          if (selector.length > 15) {
            return {
                gstin: {
                    maxlength: true
                }
            };
          } else if (!re.test(selector)) {
            return {
                gstin: {
                    pattern: true
                }
            };
          }
        }
        return null;
      }

    /*validation for card member app fullName, lastName*/
    selectValidator(control: FormControl) {
        let selector = control.value;
        if (!selector || selector < 0) {
            return {
                select: {
                    required: true
                }
            }
        }
        return null;
    }

}

/**
 * Custom ErrorStateMatcher which returns true (error exists) when the parent form group is invalid and the control has been touched
 */
export class ConfirmValidParentMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return (control.parent.invalid && (control.touched || isSubmitted));
    }
}
export class FormSubmittedMatcher implements ErrorStateMatcher {
    isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
        const isSubmitted = form && form.submitted;
        return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
    }
}
