import { Injectable, Inject } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { APP_CONFIG, AppConfig } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';
import { Cache } from '../services/cache';
import { ApiService } from '../services/api-service';

@Injectable()
export class DocumentsService {

    appConfig: IAppConfig;
    documents: any;
    valid:  number = 0;
    allowedFiles: any[];

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
                private cache: Cache,
                private http: HttpClient,
                private apiService: ApiService) {

        this.appConfig = appConfig;
        this.valid = 0;
        this.allowedFiles = ['pdf', 'png', 'jpeg', 'jpg'];
    }

    validateFileToUpload(files) {
        if (!files[0]) {
            return this.apiService.commonStrings.file_extention;
        }
        let fileSize = files[0].size*5 / 1000000;
        let fileName = files[0].name.split('.')[0];
        let extention = files[0].name.split('.').pop().toLowerCase();

        if (fileSize > 5 || fileSize < 0.025) {
            if (fileSize < 0.025)
                return files[0].name + '(' + fileSize + ')' + this.apiService.commonStrings.file_minsize;
            else
                return files[0].name + '(' + fileSize + ')' + this.apiService.commonStrings.file_size;
        } else if (this.allowedFiles.indexOf(extention) == -1) {
            return this.apiService.commonStrings.file_extention;
        }
        return false;
    }

    uploadFile(base64, data) {
        data.customerId = this.cache.user && this.cache.user.customerId ? this.cache.user.customerId: 0;
        data.base64 = base64.split(',')[1];
        return this.apiService.postApi(this.appConfig.endpoints.uploadDoc, data, null);
    }

    deleteFile(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.deleteDocument, data, null).then((res: any) => {
                if (res.deleteStatus == 'Success') {
                    resolve(res.data);
                } else {
                    reject(res.errorDesc);
                }
            }, (error) => {
                console.log('Oooops!'+ error);
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    download(url, data) {
        return new Promise( (resolve, reject) => {
            this.apiService.postApi(url, data, null).then((resp: any) => {
                if (resp) {
                    if ((resp.status.statusMessage.toLowerCase() == 'success')) {
                        resolve(resp);
                    } else if (resp.Exception) {
                        reject(resp.Exception);
                    } else if  (resp.errorDesc) {
                        reject(resp.errorDesc);
                    } else {
                        reject(this.apiService.commonStrings.download_error);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                console.log('Oooops!' + error);
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    downloadFileToBrowser(fileResp) {
        if (fileResp && fileResp.file) {
            if (navigator.msSaveBlob) { // IE 10+ 
                navigator.msSaveBlob(this.b64toBlob(fileResp.file, fileResp.mimeType), fileResp.fileName);
            } else {
                const blob = this.b64toBlob(fileResp.file, fileResp.mimeType);
                const url= window.URL.createObjectURL(blob);
                const element = document.createElement('a');

                element.setAttribute('href', url);
                element.setAttribute('download', fileResp.fileName);
                element.click();
                window.URL.revokeObjectURL(url);
            }
        }
    }

    b64toBlob(b64Data, contentType) {
        console.log('Base^64 Data-', b64Data);
        contentType = contentType;
        let sliceSize = 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];

        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }

            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        console.log('Blob-', blob);
        return blob;
    }

}
