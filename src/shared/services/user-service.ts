import { Injectable, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { UserIdleService } from 'angular-user-idle';

import { APP_CONFIG, AppConfig } from '../config/app.config';
import { IAppConfig } from '../config/iapp.config';
import { Cache } from './cache';
import { ApiService } from './api-service';

@Injectable()
export class UserService {

    appConfig: IAppConfig;
    showCongratsMsg: boolean = false;
    allowThirdPartyRedirection: boolean;
    userIdleWatchStarted: boolean;
    overview: any;
    customerJourney: boolean;
    dsaJourney: boolean;
    smJourney: boolean;
    creJourney: boolean;
    ccJourney: boolean;

	constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
                private cache: Cache,
                public apiService: ApiService,
                private router: Router,
                private userIdle: UserIdleService) {
        
        this.appConfig = appConfig;
        this.cache.loggedIn = false;
        this.allowThirdPartyRedirection = false;
        this.userIdleWatchStarted = false;
        this.customerJourney = false;
        this.dsaJourney = false;
        this.smJourney = false;
        this.creJourney = false;
        this.ccJourney = false;

        this.userIdle.onTimerStart().subscribe(count => {
            console.log('Count Down is Started!', count);
        });

        this.userIdle.onTimeout().subscribe(() => {
            console.log('Time is up!')
            this.logout();
            this.router.navigate(['']);
        });
    }

    setIsCustomerJourney(what) {
        this.customerJourney = what;
    }
    isCustomerJourney() {
        return this.customerJourney;
    }

    setIsCcJourney(what) {
        this.ccJourney = what;
    }
    isCcJourney() {
        return this.ccJourney;
    }
    
    setIsDsaJourney(what) {
        this.dsaJourney = what;
    }
    isDsaJourney() {
        return this.dsaJourney;
    }

    setIsSmJourney(what) {
        this.smJourney = what;
    }
    isSmJourney() {
        return this.smJourney;
    }

    setIsCreJourney(what) {
        this.creJourney = what;
    }
    isCreJourney() {
        return this.creJourney;
    }

    getHomeUrl() {
        let user = this.cache.user;
        if (user && user.applSource) {
            if (user.applSource == 'PL' && user.checkSource == 1) {
                return ['customer-care'];
            } else if (user.applSource == 'PL') {
                return ['apply-now-personal-loan'];
            } else if (user.applSource == 'PA') {
                return ['home'];
            } else if (user.applSource == 'TML') {
                return ['apply-now'];
            } else if (user.applSource == 'CC' ) {
                return ['customer-care'];
            } else {
                return ['home'];
            }
        }
        return [''];
    }

    setCustomerId(customerId) {
        customerId ? this.cache.user.customerId = customerId : '';
    }

    getCustomerId() {
        return this.cache.user.customerId ? this.cache.user.customerId : 1;
    }

    isAdminAuthenticated() {
        let user = this.cache.user;
        if (user && user.isAdmin && user.customerId && user.customerId != '0') {
            //customerId is actually clientId
            return true;
        }
        return false;
    }

    isAuthenticated() {
        let user = this.cache.user;
        if (user && user.customerId && user.customerId != '0') {
            console.log('isAuthenticated...YES');
            this.cache.loggedIn = true;
            this.startUserSession();
            return true;
        } else {
            return false;
        }
    }

    refreshToken() {
        this.cache.user.authorization = '';
        this.cache.set('user', this.cache.user);
        
        return new Promise((resolve, reject) => {
            let data = {contactNumber: this.cache.user.contactNumber};
            this.apiService.postApi(this.appConfig.endpoints.refreshToken, data, null).then((resp: any) => {
                if (resp && resp.status && resp.status.statusMessage) {
                    if (resp.status.statusMessage && resp.status.statusMessage.toLowerCase() == 'success') {
                        this.cache.loggedIn = true;
                        this.cache.user.authorization = resp.accessToken;
                        this.cache.set('user', this.cache.user);
                        resolve(resp);
                    } else {
                        reject(resp.status.statusMessage ? resp.status.statusMessage : this.apiService.commonStrings.http_error);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                console.log('Oooops!'+ error);
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    sendOtp(data, which) {
        return new Promise((resolve, reject) => {
            let url = '';
            if (which == 'web') {
                url = this.appConfig.endpoints.getOtp;
            } else {
                url = this.appConfig.endpoints.getEmailOtp;
            }

            this.apiService.postApi(url, data, null).then((resp: any) => {
                if (resp && resp.status && resp.status.statusCode) {
                    resolve(resp);
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                console.log('Oooops!'+ error);
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    login(data, which) {
        if (!data.isRequestFromPL) {
            delete data.isRequestFromPL;
        } else {
            data.applSource = 'PL';
        }

        return new Promise((resolve, reject) => {
            let url = '';
            if (which == 'mobile') {
                url = this.appConfig.endpoints.verifyOtp;
            } else {
                url = this.appConfig.endpoints.verifyEmailOtp;
            }
            this.apiService.postApi(url, data, null).then((resp: any) => {
                if (resp && resp.status && resp.status.statusMessage) {
                    if (resp.status.statusMessage && resp.status.statusMessage.toLowerCase() == 'success') {
                        if (resp.personalInfo) {
                            if ( resp.personalInfo.customerId) {
                                this.cache.user.customerId = resp.personalInfo.customerId;
                                this.cache.user.name = resp.personalInfo.name;
                                this.cache.user.authorization = resp.accessToken;
                                this.cache.set('user', this.cache.user);
                                this.cache.loggedIn = true;
                            }
                            this.startUserSession();
                            resolve(resp);
                        } else {
                            reject("Application Not found");
                        }
                    } else if (this.apiService.isDropOff(resp)) {
                        resolve(resp);
                    } else {
                        reject(resp.status.statusMessage ? resp.status.statusMessage : this.apiService.commonStrings.http_error);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                console.log('Oooops!'+ JSON.stringify(error));
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    logout() {
        console.log("*** logout ****");
        this.cache.clear('user');
        this.cache.user = {};
        this.cache.loggedIn = false;
        this.stopUserSession(); 
    }

    getItrCaptcha(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getITRCaptcha, data, null).then((resp: any) => {
                if (resp && resp.status && resp.status.statusCode) {
                    if (resp.status.statusCode == 200) {
                        resolve(resp);
                    } else {
                        reject(resp.status.statusMessage);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    getMcaCaptcha(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getMcaCaptcha, data, null).then((resp: any) => {
                if (resp && resp.status && resp.status.statusCode) {
                    if (resp.status.statusCode == 200) {
                        resolve(resp);
                    } else {
                        reject(resp.status.statusMessage);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    verifyCaptcha(data) {
        return this.apiService.postApiCancellable(this.appConfig.endpoints.getMcaInfo, data, null);
    }

    gstGenerateOtp(data){
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.gstGenOtp, data, null).then((resp: any) => {
                if (resp && resp.status && resp.status.statusCode) {
                    if (resp.status.statusCode == 200) {
                        resolve(resp);
                    } else {
                        reject(resp.status.statusMessage);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    getDetailsByZipCode(postalCode) {
        let data = { "pincode": postalCode, "customerId": 0 };
        
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.lookups.lookupPincode, data, null).then((res: any) => {
                if (res && res.status && res.status.statusMessage) {
                    if (res.status.statusMessage && res.status.statusMessage.toLowerCase() == 'success')
                        resolve(res);
                    else
                        reject(this.apiService.commonStrings.http_error);
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    startUserSession() {
        if (!this.userIdleWatchStarted) {
            console.log("#####....Started Watching.....#####");
            this.userIdle.startWatching();
            this.userIdleWatchStarted = true;
        }
    }

    stopUserSession() {
        if (this.userIdleWatchStarted) {
            this.userIdle.stopWatching();
        }
    }

    stopUserIdleTimer() {
        if (this.userIdleWatchStarted) {
            this.userIdle.stopTimer();
        }
    }

    getUserFromCache() {
        if (this.cache.user) {
            return this.cache.user;
        }
    }

    isRedirectionAllowed() {
        return this.allowThirdPartyRedirection;
    }

    setRedirectionAllowed(what) {
        this.allowThirdPartyRedirection = what;
    }

    forgotPassword(data) {
        return new Promise((resolve, reject)=> {
            this.apiService.postApi(this.appConfig.endpoints.forgotPassword, data, null).then((resp: any) => {
                if (resp) {
                    if (resp.statusMessage && resp.statusMessage.toLowerCase() == 'success') {
                        resolve(resp);
                    } else {
                        reject(resp.statusMessage ? resp.statusMessage : this.apiService.commonStrings.http_error);
                    }
                }
            }, (error) => {
                reject(error);
            });
        }); 
    }
     
    resetPassword(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.resetPassword, data, null).then((resp:any) => {
                if (resp) {
                    if (resp.statusMessage && resp.statusMessage.toLowerCase() == 'success') {
                        resolve(resp);
                    } else {
                        reject(resp.statusMessage ? resp.statusMessage : this.apiService.commonStrings.http_error);
                    }
                }
            }, (error) => {
                reject(error);
            });
        }); 
    }

    profileUpdate(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.profileUpdate, data, null).then((resp:any) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        }); 
    }

}
