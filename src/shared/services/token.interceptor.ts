import { Injectable, Injector } from '@angular/core';
import { Router } from '@angular/router';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Observable, from , Subject, throwError  } from 'rxjs';
import { mergeMap, switchMap, catchError } from "rxjs/operators";

import { UserService } from './user-service';
import { Cache } from './cache';

@Injectable()
export class TokenInterceptor implements HttpInterceptor {

    userService: UserService;
    cache: Cache;
    refreshTokenInProgress = false;

    tokenRefreshedSource = new Subject();
    tokenRefreshed$ = this.tokenRefreshedSource.asObservable();

    constructor(private injector: Injector, 
                private router: Router) {
    }

    addAuthHeader(request) {
        if (this.cache.user.authorization) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${this.cache.user.authorization}`
                }
            });
        }
        return request;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        this.userService = this.injector.get(UserService);
        this.cache = this.injector.get(Cache);
        // Handle request
        if (!request.url.includes("generate-token")) {
            request = this.addAuthHeader(request);
        }
        return next.handle(request).pipe(
            catchError( (error) => {
            //if(!this.userService.isCustomerJourney()){
            if (error.status === 401 || error.status === 0) {
                if (request.url.includes("generate-token")) {
                    let home = this.userService.getHomeUrl();
                    //this.userService.logout();
                    //this.router.navigate(home);
                }

                return this.refreshToken().pipe(
                    mergeMap(() => {
                        request = this.addAuthHeader(request);
                        return next.handle(request);
                    }),
                    catchError( (error) => {
                        let home = this.userService.getHomeUrl();
                        //this.userService.logout();
                        //this.router.navigate(home);
                        return throwError(error);
                    })
                );
            }
            //}
            return throwError(error);
            })
        );
    }

    refreshToken() {
        if (this.refreshTokenInProgress) {
            console.log("In TOKEn refreshToken in progress");
            return new Observable(observer => {
                this.tokenRefreshed$.subscribe(() => {
                    observer.next();
                    observer.complete();
                });
            });
        } else {
            console.log("In TOKEn refreshToken in progress --- Else");
            this.refreshTokenInProgress = true;

            return from(this.userService.refreshToken())
            .pipe(
                switchMap(() => {
                    this.refreshTokenInProgress = false;
                    this.tokenRefreshedSource.next();
                    return new Observable(observer => {
                        observer.next();
                        observer.complete();
                    });
                }),
                catchError( error => {
                    let home = this.userService.getHomeUrl();
                    //this.userService.logout();
                    //this.router.navigate(home);
                    return throwError(error);
                })
            );
        }
    }
}
