import { Injectable, Injector } from '@angular/core';
import { HttpRequest, HttpHandler, HttpInterceptor } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Cache } from './cache';

@Injectable()
export class AdminTokenInterceptor implements HttpInterceptor {

  cache: Cache;

    constructor(private injector: Injector) {
    }

    addAuthHeader(request) {
        if (this.cache.user.authorization) {
            request = request.clone({
                setHeaders: {
                    Authorization: `${this.cache.user.authorization}`
                }
            });
        }
        return request;
    }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<any> {
        this.cache = this.injector.get(Cache);
        request = this.addAuthHeader(request);
        return next.handle(request);
    }
}

