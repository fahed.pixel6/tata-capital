import { Injectable } from '@angular/core';
import { Router, CanActivate, CanLoad} from '@angular/router';
import { UserService } from './user-service';

@Injectable()
export class AuthGuardService implements CanActivate, CanLoad {
    constructor(public userService: UserService,
                public router: Router) {
    }

    canActivate(): boolean {
        if (!this.userService.isAuthenticated()) {
            this.router.navigate(this.userService.getHomeUrl());
            return false;
        }
        return true;
    }

    canLoad(): boolean {
        if (!this.userService.isAuthenticated()) {
            this.router.navigate(this.userService.getHomeUrl());
            return false;
        }
        return true;
    }
}
