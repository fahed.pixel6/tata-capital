import { NgModule } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Ng2Webstorage } from 'ngx-webstorage';
import { AppMaterialModule } from './material.module';
import { NgxMyDatePickerModule } from 'ngx-mydatepicker';

import { Error404Component } from './components/error404/error-404.component';
import { AppFinancialDetailsComponent } from './components/customer/application/details/financial/financial.component';
import { AppGstDetailsComponent } from './components/customer/application/details/financial/gst/gst.component';
import { AppItrDetailsComponent } from './components/customer/application/details/financial/itr/itr.component';
import { AppProgramDetailsComponent } from './components/customer/application/details/program/program.component';
import { AppEApprovalDetailsComponent } from './components/customer/application/details/e-approval/e-approval.component';
import { AppBusinessDetailsComponent } from './components/customer/application/business-details/business-details.component';
import { AppNeedMoreComponent } from './components/customer/application/need-more/need-more.component';

import { AppApplicationFormComponent } from './components/customer/application/application-form/application-form.component';
import { AppDocumentsComponent } from './components/customer/application/documents/documents.component';

import { DetailsDialogComponent } from './components/customer/application/dialog/details-dialog.component'

import { LoaderComponent } from './components/loader.component';

import { IndianCurrencyPipe, floatPipe } from './pipe/currency-pipe';
import { PhonePipe } from './pipe/phone-pipe';
import { IndianCurrencyFormatterDirective, FloatFormatterDirective } from './directive/currency-formatter-directive';
import { PhoneFormatterDirective,MobileFormatterDirective } from './directive/phone-formatter-directive';
import { NumberOnlyDirective, AlphaOnlyDirective, AlphaNumericOnlyDirective, AlphaNumericWithSpaceOnlyDirective, AlphaWithSpacesDirective, LimitLengthDirective, AlphaNumericWithSpecialsOnlyDirective, AlphaNumericWithSpecialsAndSpaceOnlyDirective, NumberWithSlashOnlyDirective, DateFormatterDirective, DisableCopyDirective } from './directive/directives';
import { LinkifyPipe, TabKeyPipe, StatePipe, CityPipe, BankNamePipe,FormatSrNoPipe} from './pipe/common-pipes';

import { Browser } from './directive/browser';

@NgModule({
	imports: [  CommonModule,
	    		AppMaterialModule,
		        FormsModule,
		        ReactiveFormsModule,
		        HttpClientModule,
		        Ng2Webstorage,
                NgxMyDatePickerModule.forRoot(),
		    ],
  	declarations: [
				Error404Component,
				LoaderComponent,
				AppFinancialDetailsComponent,
				AppProgramDetailsComponent,
				AppEApprovalDetailsComponent,
				AppBusinessDetailsComponent,
				AppNeedMoreComponent,
				AppApplicationFormComponent,
				DetailsDialogComponent,
				AppGstDetailsComponent,
				AppItrDetailsComponent,
				AppDocumentsComponent,
				IndianCurrencyPipe,
				floatPipe,
                PhonePipe,
				BankNamePipe,
				FormatSrNoPipe,
				CityPipe,
				StatePipe,
				LinkifyPipe,
				TabKeyPipe,
				IndianCurrencyFormatterDirective,
				FloatFormatterDirective,
				PhoneFormatterDirective,
				MobileFormatterDirective,
        		NumberOnlyDirective,
        		AlphaOnlyDirective,
				AlphaNumericOnlyDirective,
				AlphaNumericWithSpaceOnlyDirective,
        		AlphaWithSpacesDirective,
        		AlphaNumericWithSpecialsOnlyDirective,
				AlphaNumericWithSpecialsAndSpaceOnlyDirective,
				NumberWithSlashOnlyDirective,
				DateFormatterDirective,
				LimitLengthDirective,
				DisableCopyDirective,
				Browser
            ],
  	exports: [  CommonModule,
  				AppMaterialModule,
		        FormsModule,
		        ReactiveFormsModule,
		        HttpClientModule,
		        Ng2Webstorage,
                NgxMyDatePickerModule,
	            Error404Component,
				AppFinancialDetailsComponent,
				AppProgramDetailsComponent,
				AppEApprovalDetailsComponent,
				AppBusinessDetailsComponent,
				AppNeedMoreComponent,
				AppApplicationFormComponent,
				AppDocumentsComponent,
				DetailsDialogComponent,
				AppGstDetailsComponent,
				AppItrDetailsComponent,
				LoaderComponent,
				IndianCurrencyFormatterDirective,
				FloatFormatterDirective,
				PhoneFormatterDirective,
				MobileFormatterDirective,
	            NumberOnlyDirective,
        		AlphaOnlyDirective,
				AlphaNumericOnlyDirective,
				AlphaNumericWithSpaceOnlyDirective,
        		AlphaWithSpacesDirective,
        		AlphaNumericWithSpecialsOnlyDirective,
				AlphaNumericWithSpecialsAndSpaceOnlyDirective,
				NumberWithSlashOnlyDirective,
				LimitLengthDirective,
				DateFormatterDirective,
				DisableCopyDirective,
				Browser,
		        IndianCurrencyPipe,
		        FloatFormatterDirective,
				floatPipe,
				PhonePipe,
				BankNamePipe,
				FormatSrNoPipe,
				CityPipe,
				StatePipe,
				TabKeyPipe,
				LinkifyPipe
        	],
    providers:[
		        IndianCurrencyPipe,
		        FloatFormatterDirective,
				floatPipe,
				PhonePipe,
				BankNamePipe,
				FormatSrNoPipe,
				CityPipe,
				StatePipe,
				TabKeyPipe,
				LinkifyPipe
			],
	entryComponents: [DetailsDialogComponent],
})
export class SharedModule {

}
