import { Pipe, PipeTransform } from '@angular/core';
import linkifyStr from 'linkifyjs/string';

import { ConstantsService } from '../services/constants-service';

@Pipe({
    name: 'formatSrNo'
})
export class FormatSrNoPipe implements PipeTransform {
    transform(value: any): any {
        value = value.toString();
        if (value) {
            let arr = value.split("");
            let format = '0' + arr[0] + '/' + '0' + arr[1] ;
            return format;
        }
    }
}

@Pipe({
    name: 'tabkeys'
})
export class TabKeyPipe implements PipeTransform {
    transform(value, args:string[]) : any {
        let keys = [];
        for (let key in value) {
            keys.push(key);
        }
        return keys;
    }
}

@Pipe({ 
    name: 'bankName' 
})
export class BankNamePipe implements PipeTransform {
    constructor(private constantsService: ConstantsService) {}

    parse(value: string): string {
        let facilityString = '';
        this.constantsService.constants.bankName.forEach(item => {
            if (item.value == value) {
                facilityString = item.key;
            }
        }, this);
        return facilityString;
    }

    transform(value: string): string {
        let facilityString = '';
        this.constantsService.constants.bankName.forEach(item => {
            if (item.key == value) {
                facilityString = item.value;
            }
        }, this);
        return facilityString;
    }
}

@Pipe({ 
    name: 'city'
})
export class CityPipe implements PipeTransform {
    constructor(private constantsService: ConstantsService) {}

    parse(value: string): string {
        let cityString = '';
        this.constantsService.constants.cities.forEach(item => {
            if (item.value == value) {
                cityString = item.key;
            }
        }, this);
        return cityString;
    }

    transform(value: string): string {
        let cityString = '';
        this.constantsService.constants.cities.forEach(item => {
            if (item.key == value) {
                cityString = item.value;
            }
        }, this);
        return cityString;
    }
}

@Pipe({ 
    name: 'state'
})
export class StatePipe implements PipeTransform {
    constructor(private constantsService: ConstantsService) {}

    parse(value: string): string {
        let stateString = '';
        this.constantsService.constants.cities.forEach(item => {
            if (item.value == value) {
                stateString = item.key;
            }
        }, this);
        return stateString;
    }

    transform(value: string): string {
        let stateString = '';
        this.constantsService.constants.states.forEach(item => {
            if (item.key == value) {
                stateString = item.value;
            }
        }, this);
        return stateString;
    }
}

@Pipe({
    name: 'linkify'
})
export class LinkifyPipe implements PipeTransform {
    transform(str: string): string {
        return str ? linkifyStr(str, {target: '_system'}) : str;
    }
}
