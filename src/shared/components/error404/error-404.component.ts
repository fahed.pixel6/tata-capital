import { Component } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { UserService } from '../../services/user-service';

@Component({
    selector: 'app-error-404',
    templateUrl: './error-404.component.html'
})

export class Error404Component {
    error: any;
    routerSubscription: any;

    constructor(private router: Router,
                public userService: UserService, 
                public route: ActivatedRoute) {

        this.routerSubscription = this.router.events.subscribe((event) => {
            if (event instanceof NavigationEnd) {
                if (event.urlAfterRedirects.indexOf('/400') !== -1) {
                    this.error = 400;
                } else if (event.urlAfterRedirects.indexOf('/401') !== -1) {
                    this.error = 401;
                } else if (event.urlAfterRedirects.indexOf('/403') !== -1) {
                    this.error = 403;
                } else if (event.urlAfterRedirects.indexOf('/500') !== -1) {
                    this.error = 500;
                } else {
                    this.error = 404;
                }
            }
        });
    }

    ngOnDestroy() {
        this.routerSubscription.unsubscribe();
    }

    goToHome() {
        this.router.navigate(this.userService.getHomeUrl());
    }
}
