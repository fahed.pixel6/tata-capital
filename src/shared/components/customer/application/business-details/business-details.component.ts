import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { empty } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

import { Cache } from '../../../../services/cache';
import { CustomValidators, FormSubmittedMatcher, ConfirmValidParentMatcher } from '../../../../services/custom-validators';
import { UserService } from '../../../../services/user-service';
import { ApiService } from '../../../../services/api-service';
import { AlertService } from '../../../../services/alert-service';
import { ConstantsService } from '../../../../services/constants-service';

import { DMYDateFormater, DateFormater } from '../../../../models/address';

import { CustomerService } from '../../../../components/customer/services/customer-service';
import * as moment from 'moment';
import { AdobeService } from '../../services/adobe-service';


@Component({
    selector: 'app-business-details',
    templateUrl: './business-details.component.html'
})

export class AppBusinessDetailsComponent {

    detailsFormGroup: FormGroup;
    public myApplicantDob: INgxMyDpOptions;
    public myCoApplicantDob: INgxMyDpOptions;
    formSubmitted: boolean;
    panMessage: any;
    captchaInfo: any;
    captchaAPICall: any;
    showCaptchaPopup: boolean;

    showProfessionalFields: boolean;
    showDoctorFields: boolean;
    showCompanyFields: boolean;
    showCAFields: boolean;
    showSoleProprietor: boolean;
    showPartnership: boolean;
    showCaptchaVerifyFlag: boolean;

    listingLoader: boolean;
    captchaLoader: boolean;

    showCoApplicantDetails: boolean;

    cityList: any;
    showCitySelection: boolean;
    showRefCitySelection: any;
    citySelected: boolean;
    toolTipShow: boolean;

    constructor(private fb: FormBuilder,
                public userService: UserService,
                public apiService: ApiService,
                public cache: Cache,
                private alert: AlertService,
                public constantsService: ConstantsService,
                private router: Router,
                private adobeService: AdobeService,
                public customerService: CustomerService,
                private customValidators: CustomValidators,
                public parentErrorStateMatcher: ConfirmValidParentMatcher,
                public formSubmittedMatcher: FormSubmittedMatcher) {

        this.myApplicantDob = {
            dateFormat: 'dd/mm/yyyy',
            appendSelectorToBody: true,
            disableSince: this.getCurrentDate(21),
            disableUntil: this.getMaxDate(70)
        };

        this.myCoApplicantDob = {
            dateFormat: 'dd/mm/yyyy',
            appendSelectorToBody: true,
            disableSince: this.getCurrentDate(21),
            disableUntil: this.getMaxDate(70)
        };
        this.createForm();
        this.formSubmitted = false;
        this.showCaptchaPopup = false;
        this.customerService.getBusinessDetailsConstants();
        this.captchaInfo = {"step": 0, "list": '', "cin": '', "url":'', "txnId": ''};
        this.listingLoader = false;
        this.captchaLoader = false;

        this.showDoctorFields = false;
        this.showCAFields = false;
        this.showCompanyFields = false;
        this.showSoleProprietor = false;
        this.showPartnership = false;

        this.showCaptchaVerifyFlag = true;

        this.showCoApplicantDetails = false;
console.log(this.cache.user.panConstitutionsType);
console.log(this.cache.user.segmentBusiness);

    }

    getCurrentDate(pastYear){
        let currentDay = new Date();

        let startYear = pastYear ? (currentDay.getFullYear()-pastYear) : currentDay.getFullYear();
        return {year: startYear, month: currentDay.getMonth()+1, day: currentDay.getDate()}
    }

    getMaxDate(maxYear){
        let currentDay = new Date();

        let startYear = maxYear ? (currentDay.getFullYear()-maxYear) : currentDay.getFullYear();
        return {year: startYear, month: currentDay.getMonth()+1, day: currentDay.getDate()}
    }

    getDOBDefaultMonth(pastYear) {
        const currentDay = new Date();
        let month: any = currentDay.getMonth() + 1;
        if (month <= 9) {
            month = '0' + month;
        }
        return month + '/' + (pastYear ? (currentDay.getFullYear() - pastYear) : currentDay.getFullYear());
    }
        
    createForm(){
        this.detailsFormGroup = this.fb.group({
            entityName: ['', Validators.required],
            applicantName: ['', Validators.required],
            applicantDob: ['', Validators.required],
            applicantPan: ['', this.customValidators.personPanValidator],
            gender: ['', Validators.required],
            email: ['', this.customValidators.emailValidator],
            addressLine01: ['', Validators.required],
            addressLine02: ['', Validators.required],
            cityName: ['', Validators.required],
            cityId: [''],
            pinCode: ['', this.customValidators.postalCodeValidator],
            lastFYDepriciation: [''],
            industryType: ['', Validators.required],
            lastFYTurnover: ['', [Validators.required, Validators.min(100000)]],
            lastFYPAT: ['', this.fyPatValidation.bind(this)],
            yearsInBusiness: ['', Validators.required],
            resiOfficeOwnership: ['', Validators.required],
            coappDetailsList: this.fb.array([]),

            lastFYGross: ['', [Validators.required, Validators.min(100000)]],
            last1FYGrossRcpt: ['', [Validators.required, Validators.min(100000)]],
            otherIncome: ['', Validators.required],
            emiPaid: ['', Validators.required],

            registrationNo: [null],
            stateMedicalCouncil: [''],
            universityName: [''],
            
            captchaText: ['']
        });
        this.detailsFormGroup.get('lastFYTurnover').valueChanges.subscribe((value)=>{
            this.detailsFormGroup.get('lastFYPAT').updateValueAndValidity();
        })
    }
 
    ngOnInit() {
        this.customerService.getBusinessDetails().then((response: any) => {
console.log(response);
            if(response.personalInfo && response.personalInfo.name){
                this.cache.user.firstName = response.personalInfo.name ? response.personalInfo.name : this.cache.user.firstName;
            }
            this.detailsFormGroup.patchValue(response, {emitEvent: false});

            this.cache.user.panConstitutionsType = response.constitutionType;
            this.cache.user.segmentBusiness = response.segmentOfBusiness;
            this.cache.set('user', this.cache.user);

            if(this.cache.user.panConstitutionsType == "Professional"){
                if(this.cache.user.segmentBusiness == '5'){
                    this.showDoctorFields = true;
                }else if(this.cache.user.segmentBusiness == '6' || this.cache.user.segmentBusiness == '7'){
                    this.showCAFields = true;
                }
            }else if(this.cache.user.panConstitutionsType == "Private Company" || this.cache.user.panConstitutionsType == "Partnership Company"){
                this.showCompanyFields = true;
            }else if(this.cache.user.panConstitutionsType == "Proprietorship"){
                this.showSoleProprietor = true;
            }
    
            if(!this.showDoctorFields){
                this.detailsFormGroup.get('universityName').disable();
                this.detailsFormGroup.get('stateMedicalCouncil').disable();
            }
            if(this.showDoctorFields || this.showCAFields){
                this.detailsFormGroup.get('lastFYTurnover').disable();
                this.detailsFormGroup.get('lastFYPAT').disable();
                this.detailsFormGroup.get('lastFYDepriciation').disable();
                this.detailsFormGroup.get('entityName').disable();
                this.detailsFormGroup.get('industryType').disable();
            }
            if(this.showSoleProprietor){
                this.detailsFormGroup.get('lastFYGross').disable();
                this.detailsFormGroup.get('last1FYGrossRcpt').disable();
                this.detailsFormGroup.get('otherIncome').disable();
                this.detailsFormGroup.get('emiPaid').disable();
                this.detailsFormGroup.get('registrationNo').disable();
                this.detailsFormGroup.get('entityName').disable();

                
                    
                    if(response.coappDetailsList && response.coappDetailsList.length){
                            response.coappDetailsList.forEach((item) => {
                                if(item && item.coApplicantName){
                                    this.showCoApplicantDetails = true;
                                    this.initcoApplicantDetails(response.coappDetailsList);
                                }
                            });
                    }else{
                        this.showCoApplicantDetails = false;
                    }
                
                
                
            }
            if(this.showCompanyFields){
                this.detailsFormGroup.get('lastFYGross').disable();
                this.detailsFormGroup.get('last1FYGrossRcpt').disable();
                this.detailsFormGroup.get('emiPaid').disable();
                this.detailsFormGroup.get('registrationNo').disable();
                this.detailsFormGroup.get('otherIncome').disable();

                if(response.coappDetailsList && response.coappDetailsList.length){
                    response.coappDetailsList.forEach((item) => {
                        if(item && item.coApplicantName){
                            this.showCoApplicantDetails = true;
                            this.initcoApplicantDetails(response.coappDetailsList);
                        }
                    });
                }else{
                    this.showCoApplicantDetails = false;
                }
            }

            if(response.applicantDob){
                this.detailsFormGroup.get('applicantDob').patchValue(new DateFormater(response.applicantDob));
            }
            if(response.applicantName){
                this.detailsFormGroup.get('applicantName').disable();
            }
            if(response.applicantPan){
                this.detailsFormGroup.get('applicantPan').disable();
            }
            if(response.cityName){
                this.detailsFormGroup.get('cityName').disable({emitEvent: false});
            }

            if(response.constitutionType == "Private Company" || response.constitutionType == "Partnership Company"){
                this.detailsFormGroup.get('entityName').patchValue(response.entityName);
                this.detailsFormGroup.get('entityName').disable();
            }else{
                this.detailsFormGroup.get('entityName').disable();
            }
            
            
		}, (error) => {
			this.alert.error(error);
			this.formSubmitted = false;
		});
        this.onChanges();
    }

    onChanges(){

        this.detailsFormGroup.get('cityName').valueChanges
        .pipe(debounceTime(500), 
            switchMap(value =>  { console.log(value); return this.getPerCityNames(value) } )
        ).subscribe((resp:any) => {
            if(resp.citiesNames && resp.citiesNames.length && resp.status.statusCode == "200"){
                this.cityList = resp.citiesNames;
                this.showCitySelection = true;
            }
        }, error => {
            this.alert.error(error.status.statusMessage);
        });

        this.detailsFormGroup.get('applicantPan').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value => this.checkPanInfo(value) )
            ).subscribe((resp:any) => {
                console.log(resp);
                if(resp.status.statusCode == '200'){
                    if(resp.isValidPAN){
                        this.panMessage = resp.panStatus;
                    }
                }else{
                    this.panMessage = '';
                    this.detailsFormGroup.get('applicantPan').setErrors({pan: {'incorrect': true}});
                }
            }, error => {
                console.log(this.detailsFormGroup.get('applicantPan').value);
                this.panMessage = '';
                this.detailsFormGroup.get('applicantPan').setErrors({pan: {'incorrect': true}});
            });

        this.detailsFormGroup.get('registrationNo').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value => this.checkMembershipId(value) )
            ).subscribe((resp:any) => {
                console.log(resp);
                if(resp.status.statusCode == '200'){
                    console.log(resp.status.statusMessage);
                }
            }, error => {
                this.alert.error(error);
            });

        this.detailsFormGroup.get('registrationNo').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value => this.checkDoctorVerification(value) )
            ).subscribe((resp:any) => {
                if(resp.status.statusCode == '200'){
                    console.log(resp.status.statusMessage);
                }
            }, error => {
                this.alert.error(error);
            });

        this.detailsFormGroup.get('stateMedicalCouncil').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value => this.checkDoctorVerification(value) )
            ).subscribe((resp:any) => {
                if(resp.status.statusCode == '200'){
                    console.log(resp.status.statusMessage);
                }
            }, error => {
                this.alert.error(error);
            });

        this.detailsFormGroup.get('universityName').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value => this.checkDoctorVerification(value) )
            ).subscribe((resp:any) => {
                if(resp.status.statusCode == '200'){
                    console.log(resp.status.statusMessage);
                }
            }, error => {
                this.alert.error(error);
            });
    }

    getPerCityNames(value){
        
        if(!value){
            this.citySelected = false;
            this.showCitySelection = false;
            return empty();
        }
        if(this.citySelected) return empty();

        this.showCitySelection = false;

        if(value && value.length > 2){
            return this.customerService.getCityNames({cityName: value});
        }else{
            return empty();
        }
        
    }

    selectPerCity(obj){
        this.showCitySelection = false;
        this.citySelected = true;
        if(obj.key){
            this.detailsFormGroup.get('cityId').patchValue(obj.key);
        }
        this.detailsFormGroup.get('cityName').patchValue(obj.value);
    }


    submit(){ 
        let data = this.detailsFormGroup.getRawValue();
        data.applicantDob = this.apiService.transformDate(data.applicantDob);

        data.coappDetailsList.forEach((item , index) => {
            item.coApplicantId = index + 1;
            item.coApplicantDob = this.apiService.transformDate(item.coApplicantDob);
            delete item.citySelected;
            delete item.showCitySelection;
            delete item.panMessage;
        });
        delete data.captchaText;

        let start = new Date();
        let now = start.getDay() + "/" +start.getMonth()+"/"+start.getFullYear();
        let today = moment(now,'DD/MM/YYYY');
        let end = moment(data.applicantDob, 'DD/MM/YYYY'); // another date
        let duration = moment.duration(today.diff(end));
        let years = duration.asYears();

        if(!this.showDoctorFields){
            delete data.stateMedicalCouncil;
            delete data.universityName;
        }
        if(this.showSoleProprietor){
            delete data.lastFYGross;
            delete data.last1FYGrossRcpt;
            delete data.otherIncome;
            delete data.emiPaid;
            delete data.registrationNo;
            delete data.entityName;

            if(years > 65 && !this.showCoApplicantDetails){
                this.alert.success('Your age is greater than 65, So please enter Co-applicant details');
                this.showCoApplicantDetails = true;
                this.addNewCoApplicant();
                return;
            }
        }

        
        if(this.showCompanyFields){
            delete data.registrationNo;
            delete data.otherIncome;
            delete data.lastFYGross;
            delete data.last1FYGrossRcpt;
            delete data.otherIncome;
            delete data.emiPaid;
        }

        if(this.showDoctorFields || this.showCAFields){
            delete data.lastFYTurnover;
            delete data.lastFYPAT;
            delete data.lastFYDepriciation;
            delete data.entityName;
            delete data.industryType;
        }


        if((this.detailsFormGroup.valid || this.detailsFormGroup.disabled) && !this.formSubmitted){
            this.adobeService.callSatellite('bl-business-detail-continue');
            this.formSubmitted = true;
            this.customerService.saveBusinessDetails(data).then((response: any) => {
                if(response.status.statusCode == '774'){
                    if(this.cache.user.panConstitutionsType != "Professional"){
                        this.alert.error(response.status.statusMessage);
                        this.showCoApplicantDetails = true;
                        this.addNewCoApplicant();
                        this.detailsFormGroup.get('coappDetailsList').enable({emitEvent: false});
                    }else{
                        this.afterDropoff(response);
                    }
                }else if(response.status.statusCode == '701'){
                    this.alert.error(response.status.statusMessage);
                }else if(this.apiService.isDropOff(response)){
                    this.afterDropoff(response);
                }else{
                    if(this.showDoctorFields || this.showCAFields){
                        this.router.navigateByUrl('/application/details/e-approval');
                    }else{
                        this.router.navigateByUrl('/application/details/financial');
                    }
                }
                this.formSubmitted = false;
            }, (error) => {
                this.alert.error(error);
                this.formSubmitted = false;
            });
        }else{
            if(!this.detailsFormGroup.valid){
                this.alert.error('Please fill all the required fields');
            }
        }
    }
    initcoApplicantDetails(collection){
        const control = <FormArray>this.detailsFormGroup.get('coappDetailsList');
        if(collection && collection.length){
            collection.forEach((row , index) => {
                control.push(this.initcoApplicantDetailsRow(row, index));
            });
        }
    }
    addNewCoApplicant() {
        const control = <FormArray>this.detailsFormGroup.get('coappDetailsList');
        control.push(this.initcoApplicantDetailsRow(null, control.length));
    }

    initcoApplicantDetailsRow(data, index) {
        let group = this.fb.group({
            coApplicantId : [data && data.coApplicantId ? data.coApplicantId : index+1],
            coApplicantDob : [data && data.coApplicantDob ? new DateFormater(data.coApplicantDob) : '', Validators.required], 
            coApplicantName: [data && data.coApplicantName ? data.coApplicantName : '', Validators.required],
            applicantPan : [data && data.applicantPan ? data.applicantPan : '', this.customValidators.personPanValidator],
            gender: [data && data.gender ? data.gender : '', Validators.required], 
            mobileNumber:[data && data.mobileNumber ? data.mobileNumber : '', this.customValidators.mobileValidator],
            panMessage: [''],
            citySelected: [''],
            showCitySelection: [''],     
            addressLine01: [data && data.addressLine01 ? data.addressLine01 : '', Validators.required],
            addressLine02 : [data && data.addressLine02 ? data.addressLine02 : '', Validators.required], 
            cityName: [data && data.cityName ? data.cityName : '', Validators.required],
            cityId: [data && data.cityId ? data.cityId :''],
            pinCode : [data && data.pinCode ? data.pinCode : '', this.customValidators.postalCodeValidator],
        });

        if(data && data.coApplicantName){
            group.get('coApplicantName').disable();
        }
        if(data && data.applicantPan){
            group.get('applicantPan').disable();
        }

        group.get('applicantPan').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value => this.checkCoApplicantPanInfo(group, value) )
            ).subscribe((resp:any) => {
                console.log(resp);
                if(resp.status.statusCode == '200'){
                    if(resp.isValidPAN){
                        group.get('panMessage').patchValue(resp.panStatus);
                    }
                }else{
                    group.get('panMessage').patchValue('');
                    group.get('applicantPan').setErrors({pan: {'incorrect': true}});
                }
            }, error => {
                group.get('panMessage').patchValue('');
                group.get('applicantPan').setErrors({pan: {'incorrect': true}});
            });

        group.get('cityName').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value => this.getCityNames(group, value) )
                ).subscribe((resp:any) => {
                    if(resp.citiesNames && resp.citiesNames.length && resp.status.statusCode == "200"){
                        this.cityList = resp.citiesNames;
                        group.get('showCitySelection').patchValue(true);
                    }
                }, error => {
                    this.alert.error(error.status.statusMessage);
                });

        return group;
    }

    getCityNames(group, value){
        if(!value || group.get('citySelected').value == true){
            group.get('citySelected').patchValue(false);
            group.get('showCitySelection').patchValue(false);
            return empty();
        }

        group.get('showCitySelection').patchValue(false);

        if(value && value.length > 2){
            return this.customerService.getCityNames({cityName: value});
        }else{
            return empty();
        }
        
    }

    selectCity(obj , group , index){
        group.get('citySelected').patchValue(true);
        group.get('showCitySelection').patchValue(false);
        if(obj.key){
            group.get('cityId').patchValue(obj.key);
        }
        group.get('cityName').patchValue(obj.value);
    }

    getCityNameDisplayFn(val?: any){
        return (val) => this.displayCityName(val);
    }

    displayCityName(key) {
        let matchedValue:any = '';
        if(this.cityList && this.cityList.length){
            this.cityList.forEach((item) => {
                if(item.key == key)
                    matchedValue = item.value;
            });
        }
        return matchedValue;
    }

    cityShowTooltip(){
        this.toolTipShow = true;
    }

    cityHideTooltip(){
        setTimeout(() => {
            this.showCitySelection = false;
        }, 1000);
        this.toolTipShow = false;
    }

    deleteApplicant(index){
        const control = <FormArray>this.detailsFormGroup.get('coappDetailsList');
        control.removeAt(index);
    }

    checkCoApplicantPanInfo(group, value){
        if(!value || !group.get('applicantPan').valid){
            group.get('panMessage').reset();
            return empty();
        }

        if(value && !group.get('applicantPan').valid){
            group.get('panMessage').reset();
        }

        return this.customerService.getDetailsByPan({ panNumber: value , coApplicantId: group.get('coApplicantId').value, isBusinessDetails: true});
    }

    checkPanInfo(value){
        if(!value || !this.detailsFormGroup.get('applicantPan').valid){
            return empty();
        }

        if(value && !this.detailsFormGroup.get('applicantPan').valid){
            this.detailsFormGroup.get('panMessage').reset();
        }

        return this.customerService.getDetailsByPan({ panNumber: value, isBusinessDetails: true });
    }

    checkMembershipId(value){
        if(!value && !this.detailsFormGroup.get('registrationNo').valid){
            return empty();
        }
        
        if(this.cache.user.segmentBusiness == '6'){
            return this.customerService.getMembershipCAValidate({membershipId: value});
        }else if(this.cache.user.segmentBusiness == '7'){
            return this.customerService.getMembershipCOAValidate({membershipId: value});
        }else{
            return empty();
        }
        
    }

    checkDoctorVerification(value){
        if(!value){
            return empty();
        }
        if(this.detailsFormGroup.get('registrationNo').valid && this.detailsFormGroup.get('stateMedicalCouncil').valid && this.detailsFormGroup.get('universityName').valid){
            return this.customerService.getDoctorVerificationService({
                stateMedicalCouncil: this.detailsFormGroup.get('stateMedicalCouncil').value,
                regdNo: this.detailsFormGroup.get('registrationNo').value,
                universityName: this.detailsFormGroup.get('universityName').value
            });
        }else{
            return empty();
        }
    }

    selectCompanyName(item){
        this.captchaInfo.step = 2;
        this.detailsFormGroup.get('entityName').patchValue(item.companyName);
        this.captchaInfo.cin = item.companyId;
    }

    openCompanyPopup(){

        this.listingLoader = true;
        this.captchaLoader = false;

        this.detailsFormGroup.get('captchaText').reset();
        let data = {
            companyName: this.detailsFormGroup.get('entityName').value,
            action: "CAPTCHA"
        }
        this.userService.getMcaCaptcha(data).then((resp:any) => {
			this.listingLoader = false;
			this.captchaInfo.step = 1;
			this.captchaInfo.list = resp.companiesList;
			this.captchaInfo.url = resp.captchaUrl;
			this.captchaInfo.txnId = resp.captchaTxnId;
        }, (error) =>{
			this.listingLoader = false;
			this.captchaInfo.step = 0;
			//this.alert.error(error);
        });
    }

    verifyCaptcha(){

        if(this.captchaLoader) return;

        this.captchaAPICall = this.userService.verifyCaptcha({
            companyName: this.detailsFormGroup.get('entityName').value,
            companyCin: this.captchaInfo.cin,
            captchaCode: this.detailsFormGroup.get('captchaText').value,
            captchaTxnId: this.captchaInfo.txnId,
            action: 'RESULT'
        }).subscribe((resp: any) => {
            if (resp && resp.status && resp.status.statusCode == 200) {
                this.captchaInfo.step = 0;
                this.captchaLoader = false;
                this.showCaptchaVerifyFlag = true;
            }
        });
    }

    refreshCaptcha() {
        if (this.captchaAPICall) this.captchaAPICall.unsubscribe();
        this.captchaInfo.step = 2;
        this.captchaLoader = true;
        this.captchaInfo.url = '';
        this.detailsFormGroup.get('captchaText').reset();
        this.userService.getMcaCaptcha(this.detailsFormGroup.get('entityName').value).then((resp: any) => {
            this.captchaLoader = false;
            this.captchaInfo.list = resp.companiesList;
			this.captchaInfo.url = resp.captchaUrl;
			this.captchaInfo.txnId = resp.captchaTxnId;
        }, function (error) {
            this.captchaLoader = false;
            this.captchaInfo.step = 0;
            //this.alert.error(error);
        });
    }

    closePopup(){
        this.captchaInfo.step = 0;
        this.detailsFormGroup.get('captchaText').reset();
    }

    afterDropoff(res){
        this.customerService.dropOffError = res.status.statusMessage;
        this.customerService.dropOffErrorReason = res.status.dropOffPoint;
        this.customerService.dropOffStatusCode = res.status.statusCode;
        if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
            this.router.navigateByUrl('/thanks');
        }else{
            this.router.navigateByUrl('/application/thanks');
        }
        
    }

    goBack(){
        if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
            this.router.navigateByUrl('/home');
        }
	}

    exit(){
		this.router.navigateByUrl('/home')
	}

    fyPatValidation(control: FormControl){
        if (!control || !control.parent) return null;
        let group = control.parent;
        if(group){
            let lastFYPAT = control.value.toString().replace(/\D/g, '');
            if(!lastFYPAT){
                return { pat: { required: true }};
            }
            if(lastFYPAT < 100000){
                return { pat: { min: true }};
            }
            
            let lastFYTurnover = group.get('lastFYTurnover').value.toString().replace(/\D/g, '');
            if(lastFYTurnover){
                if(Number(lastFYPAT) > Number(lastFYTurnover)){
                    return { pat: { more: true }};

                }
                
            }
        }
      
        return null;
    }

    goToBasicDetail(){
        this.router.navigate(['/'], { queryParams: { showthirdStep: 'true' } });
    }

}
