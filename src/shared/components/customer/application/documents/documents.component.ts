
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { INgxMyDpOptions } from 'ngx-mydatepicker';

import { empty } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

import { Cache } from '../../../../services/cache';
import { CustomValidators, FormSubmittedMatcher, ConfirmValidParentMatcher } from '../../../../services/custom-validators';
import { UserService } from '../../../../services/user-service';
import { ApiService } from '../../../../services/api-service';
import { AlertService } from '../../../../services/alert-service';
import { ConstantsService } from '../../../../services/constants-service';

import { CustomerService } from '../../../../components/customer/services/customer-service';
import { DocumentsService } from 'shared/services/documents-service';
import { AdobeService } from '../../services/adobe-service';


@Component({
    selector: 'app-documents',
    templateUrl: './documents.component.html'
})

export class AppDocumentsComponent {

	documentsFormGroup: FormGroup;
    formSubmitted: boolean;
    stateLoader: any[];
    allowedFiles: any[];
    showGroupFields: boolean;
    showDocName: any;
    isEmailSent: any;

    kycDocList: any;
    incomeDocList: any;
    getRedirectFlag: boolean;

    countDoc: number;

    constructor(private fb: FormBuilder,
                public userService: UserService,
                public apiService: ApiService,
                public cache: Cache,
                private alert: AlertService,
                public constantsService: ConstantsService,
                public docService: DocumentsService,
                private router: Router,
                private adobeService: AdobeService,
                private route: ActivatedRoute,
                public customerService: CustomerService,
                private customValidators: CustomValidators,
                public parentErrorStateMatcher: ConfirmValidParentMatcher,
                public formSubmittedMatcher: FormSubmittedMatcher) {

        this.formSubmitted = false;
        this.showGroupFields = false;
        this.getRedirectFlag = false;
        this.isEmailSent = false;
        this.createForm();
        this.allowedFiles = ['pdf', 'png', 'jpeg', 'jpg'];
        this.stateLoader = new Array();
        this.showDocName = new Array();
        this.countDoc = 0;
        
    
        this.customerService.getDocumentsConstants();

        this.route.queryParams.subscribe(params => {

            if(params && params.dtls){
                let docString: any = atob(params.dtls);
                let docData: any = this.parseQuery(docString);     
                let customerId: any = docData.cid;
                let contactNumber: any = docData.contactNumber;
				if(customerId && contactNumber){
console.log(customerId, contactNumber);
                    this.getRedirectFlag = true;
                    this.cache.user.customerId = customerId;
                    this.cache.user.contactNumber = contactNumber;
                    this.cache.set('user', this.cache.user);
                    this.constantsService.getAllDocumentList();
				}                
            }
        });
    }

    parseQuery(search) {

        var args = search.split('&');
        var argsParsed = {};
        var i, arg, kvp, key, value;
        for (i=0; i < args.length; i++) {
            arg = args[i];
            if (-1 === arg.indexOf('=')) {

                argsParsed[decodeURIComponent(arg).trim()] = true;
            }else {

                kvp = arg.split('=');
                key = decodeURIComponent(kvp[0]).trim();
                value = decodeURIComponent(kvp[1]).trim();
                argsParsed[key] = value;
            }
        }
        return argsParsed;
    }

    ngOnInit() {

        this.customerService.getDocumentDetails().then((response: any) => {
console.log(response);
            if(response.personalInfo && response.personalInfo.name){
                this.cache.user.firstName = response.personalInfo.name ? response.personalInfo.name : this.cache.user.firstName;
            }
            if(response.government_registration_proof){
                this.documentsFormGroup.get('government_registration_proof').patchValue(response.government_registration_proof[0].docName);
            }
            if(response.address_proof_office){
                this.documentsFormGroup.get('address_proof_office').patchValue(response.address_proof_office[0].docName);
            }
            if(response.address_proof_office){
                this.documentsFormGroup.get('address_proof_residence').patchValue(response.address_proof_residence[0].docName);
            }
            if(response.ownership_proof){
                this.documentsFormGroup.get('ownership_proof').patchValue(response.ownership_proof[0].docName);
            }

            if(response.kyc_doc){
                this.kycDocList = response.kyc_doc;

                this.kycDocList.forEach((item) => {
                    if(item.docName){
                        this.countDoc++;
                    }
                });

            }else{
                setTimeout(() => {
                    this.kycDocList = this.constantsService.constants.customerDocumentList.kyc_doc;
                }, 500);
            }

            if(response.income_doc){
                this.incomeDocList = response.income_doc;
                this.incomeDocList.forEach((item) => {
                    if(item.docName){
                        this.countDoc++;
                    }
                });
            }else{
                setTimeout(() => {
                    this.incomeDocList = this.constantsService.constants.customerDocumentList.income_doc;
                }, 500);
            }
        });
    }


    createForm(){
        this.documentsFormGroup = this.fb.group({
            government_registration_proof: ['', Validators.required],
            address_proof_office: ['', Validators.required],
            address_proof_residence: ['', Validators.required],
            incomeDoc: ['', Validators.required],
            kyc_doc: ['', Validators.required],
            ownership_proof:['', Validators.required],
        });
    }


    submit(){
        if(this.getRedirectFlag == true){
            this.router.navigate(['/application/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
        }else{
            this.router.navigateByUrl('/application/application-form'); 
        }
		
    }
    uploadDocuments(files: FileList, elId: string, fieldName: string) {

        let fileName = files[0].name;
        let fileSize = files[0].size;

        if (!this.validateFileToUpload(files)) {
            return;
        }
        if(fileSize > 5242880){
            this.alert.error("File size should not be greater than 5MB");
            return false;
        }

        this.stateLoader[elId] = true;

        let data = {
            docUploadName: fileName,
            docUploadType: fieldName,
        };
        
        var reader = new FileReader();
        reader.onload = ((theFile) => {
            return (e) => {
                this.docService.uploadFile(e.target.result, data).then( (resp: any) => {
                    if(resp.status.statusCode == 200){
                        this.adobeService.callSatellite('bl-upload-document-success');
                        this.documentsFormGroup.get(elId).patchValue(fileName);
                        this.stateLoader.splice(this.stateLoader.findIndex(x => x == elId), 1);
                    }else{
                        this.stateLoader[elId] = false;
                        this.alert.error(resp.status.statusMessage);
                    }
                },  (error) => {
                    //show popup with error..
                    this.stateLoader[elId] = false;
                    this.alert.error(error);
        
                });
            };
        })(files.item(0));
        reader.readAsDataURL(files.item(0));
    }
    uploadSingleDocument(files: FileList, item: any, index) {

        let fileName = files[0].name;
        let fileSize = files[0].size;
        let elId = item.key ;

        const docUploadType = item.value;
        if (!this.validateFileToUpload(files)) {
            return;
        }
        if(fileSize > 5242880){
            this.alert.error("File size should not be greater than 5MB");
            return false;
        }

        this.stateLoader[elId] = true;

        let data = {
            docUploadName: fileName,
            docUploadType: docUploadType,
        };
        
        var reader = new FileReader();
        reader.onload = ((theFile) => {
            return (e) => {
                this.docService.uploadFile(e.target.result, data).then( (resp: any) => {
                    if(resp.status.statusCode == 200){
                        this.adobeService.callSatellite('bl-upload-document-success');
                        this.showDocName[item.key] = fileName;
                        this.stateLoader.splice(this.stateLoader.findIndex(x => x == elId), 1);
                        this.countDoc++;
                        console.log(this.countDoc);
                    }else{
                        this.stateLoader[elId] = false;
                        this.alert.error(resp.status.statusMessage);
                    }
                },  (error) => {
                    //show popup with error..
                    this.stateLoader[elId] = false;
                    this.alert.error(error);
        
                });
            };
        })(files.item(0));
        reader.readAsDataURL(files.item(0));
    }
    removeDocument(docEle){
        this.stateLoader[docEle] = false;
        this.documentsFormGroup.get(docEle).reset();
    }
    goBack(){
        if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
            this.router.navigateByUrl('/application/details/e-approval');
        }
    }
    exit(){
		this.router.navigateByUrl('/home')
	}
    removeSingleDocument(index){
        this.stateLoader[index] = false;
        this.showDocName[index] = '';
        this.countDoc--;
        console.log(this.countDoc);
    }
    skip(){
        this.adobeService.callSatellite('skip-document-upload');
        this.router.navigateByUrl('/application/application-form'); 
    }

    validateFileToUpload(files) {
        
        if (!files[0]) {
            return false;
        }
        let fileSize = files[0].size / 1000000;
        let fileName = files[0].name.split('.')[0];
        let extention = files[0].name.split('.').pop().toLowerCase();

        if (fileSize > 10) {
            this.alert.error(files[0].name + '(' + fileSize + ')' + this.apiService.commonStrings.file_size);
            return false;
        } else if (this.allowedFiles.indexOf(extention) == -1) {
            this.alert.error(this.apiService.commonStrings.file_extention);
            return false;
        }
        return true;
    }

    sendDocUploadLink(){
        let data = {serviceType: "DOCUMENT_UPLOAD", profile: this.cache.user.profile};
        this.adobeService.callSatellite('bl-upload-document-click');
        this.customerService.sendSms(data).then((response: any) => {
            if(response.status && response.status.statusCode ){
                this.isEmailSent = true;
            }
        },(error) => {
            this.isEmailSent = false;
            this.alert.error(error);
        });
    }

}
