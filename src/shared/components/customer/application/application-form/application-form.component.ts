import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { empty } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

import { Cache } from '../../../../services/cache';
import { CustomValidators, FormSubmittedMatcher, ConfirmValidParentMatcher } from '../../../../services/custom-validators';
import { UserService } from '../../../../services/user-service';
import { ApiService } from '../../../../services/api-service';
import { AlertService } from '../../../../services/alert-service';
import { ConstantsService } from '../../../../services/constants-service';
import { DMYDateFormater, DateFormater } from '../../../../models/address';

import { CustomerService } from '../../../../components/customer/services/customer-service';
import { AdobeService } from '../../services/adobe-service';


@Component({
    selector: 'app-application-form',
    templateUrl: './application-form.component.html'
})

export class AppApplicationFormComponent {

    applicationFormGroup: FormGroup;
    formSubmitted: boolean;
    public myApplicantDob: INgxMyDpOptions;

    cityList: any;
    showCitySelection: boolean;
    showRefCitySelection: any;
    citySelected: boolean;
    refCitySelected: any;
    toolTipShow: boolean;

    constructor(private fb: FormBuilder,
                public userService: UserService,
                public apiService: ApiService,
                public cache: Cache,
                private alert: AlertService,
                public constantsService: ConstantsService,
                private router: Router,
                private adobeService: AdobeService,
                public customerService: CustomerService,
                private customValidators: CustomValidators,
                public parentErrorStateMatcher: ConfirmValidParentMatcher,
                public formSubmittedMatcher: FormSubmittedMatcher) {

            this.formSubmitted = false;
            this.createForm();
            this.customerService.getApplicationFormConstants();
            this.showRefCitySelection = [];
            this.refCitySelected = [];

            this.myApplicantDob = {
                dateFormat: 'dd/mm/yyyy',
                appendSelectorToBody: true,
                disableSince: this.getCurrentDate(21),
                disableUntil: this.getMaxDate(70)
            };

    }

    getCurrentDate(pastYear){
        let currentDay = new Date();

        let startYear = pastYear ? (currentDay.getFullYear()-pastYear) : currentDay.getFullYear();
        return {year: startYear, month: currentDay.getMonth()+1, day: currentDay.getDate()}
    }

    getMaxDate(maxYear){
        let currentDay = new Date();

        let startYear = maxYear ? (currentDay.getFullYear()-maxYear) : currentDay.getFullYear();
        return {year: startYear, month: currentDay.getMonth()+1, day: currentDay.getDate()}
    }
    getDOBDefaultMonth(pastYear) {
        const currentDay = new Date();
        let month: any = currentDay.getMonth() + 1;
        if (month <= 9) {
            month = '0' + month;
        }
        return month + '/' + (pastYear ? (currentDay.getFullYear() - pastYear) : currentDay.getFullYear());
    }

    createForm(){
  
        this.applicationFormGroup = this.fb.group({
            applicantName: ['', Validators.required],
            dob: ['', Validators.required],
            mothersMaidenName: ['', Validators.required],
            emailId: ['', this.customValidators.emailValidator],
            gender: ['', Validators.required],
            marritalStatus: ['', Validators.required],
            addressLine1: ['', Validators.required],
            addressLine2: ['', Validators.required],
            landMark: [''],
            cityName: ['', Validators.required],
            cityId: [''],
            pinCode: ['', this.customValidators.postalCodeValidator],
            yearsInCurAddress: ['', Validators.required],
            yearsInCurCity: ['', Validators.required],
            reference: this.fb.array([])    
        });
    }

    ngOnInit() {
        this.customerService.getApplicationFormDetails().then((response: any) => {

            if(response.personalInfo && response.personalInfo.name){
                this.cache.user.firstName = response.personalInfo.name ? response.personalInfo.name : this.cache.user.firstName;
            }
            
            this.applicationFormGroup.patchValue(response, {emitEvent: false, onlySelf: true});
            if(response.reference && response.reference.length){
                this.initReferenceDetails(response.reference);
            }else{
                this.addNewReference();
            }
           
            if(response.applicantName){
                this.applicationFormGroup.get('applicantName').disable();
            }
            if(response.dob){
                this.applicationFormGroup.get('dob').patchValue(new DateFormater(response.dob));
                this.applicationFormGroup.get('dob').disable();
            }
            if(response.emailId){
                this.applicationFormGroup.get('emailId').disable();
            }
            if(response.addressLine1){
                this.applicationFormGroup.get('addressLine1').disable();
            }
            if(response.addressLine2){
                this.applicationFormGroup.get('addressLine2').disable();
            }
            if(response.cityName && response.cityId){
                this.applicationFormGroup.get('cityName').disable({emitEvent: false});
            }
            if(response.cityName && response.pinCode){
                this.applicationFormGroup.get('pinCode').disable();
            }
		}, (error) => {
			this.alert.error(error);
			this.formSubmitted = false;
        });
        
        this.onChanges();
    }
    onChanges(){
        this.applicationFormGroup.get('cityName').valueChanges
        .pipe(debounceTime(500), 
            switchMap(value =>  { console.log(value); return this.getCityNames(value) } )
        ).subscribe((resp:any) => {
            if(resp.citiesNames && resp.citiesNames.length && resp.status.statusCode == "200"){
                this.cityList = resp.citiesNames;
                this.showCitySelection = true;
            }
        }, error => {
            this.alert.error(error.status.statusMessage);
        });
    }

    continue(){
        
        
        let data = this.applicationFormGroup.getRawValue();
        data.dob = this.apiService.transformDate(data.dob);
        
        if(this.applicationFormGroup.valid || this.applicationFormGroup.disabled){
            this.formSubmitted = true;
            this.adobeService.callSatellite('bl-submit-application');
            this.customerService.saveApplicationDetails(data).then((response: any) => {
                if(response.status.statusCode == 200){
                    if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
                        this.router.navigate(['/'], { queryParams: { showSubmitSuccess: 'true' } });
                    }else{
                        this.router.navigate(['/application/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
                    }
                }else if(response.status.statusCode == 701){
                    this.alert.error(response.status.statusMessage);
                }
                this.formSubmitted = false;
            }, (error) => {
                this.alert.error(error);
                this.formSubmitted = false;
            });
        }
    } 
    exit(){
		this.router.navigateByUrl('/home')
	}
    goBack(){
        if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
            this.router.navigateByUrl('/application/documents');
        }
	}  

    initReferenceDetails(collection){
        const control = <FormArray>this.applicationFormGroup.get('reference');
        if(collection && collection.length){
            collection.forEach((row,index) => {
                control.push(this.initReferenceDetailsRow(row, index));
            });
        }
    }

    addNewReference() {
        const control = <FormArray>this.applicationFormGroup.get('reference');
        control.push(this.initReferenceDetailsRow(null, 0));
        control.push(this.initReferenceDetailsRow(null, 1));
    }

    initReferenceDetailsRow(data , index) {
        let group = this.fb.group({
            firstName: [data && data.firstName ? data.firstName : '', Validators.required],
            lastName: [data && data.lastName ? data.lastName : '', Validators.required],
            addressType: [data && data.addressType ? data.addressType : '', Validators.required],
            addressLine1: [data && data.addressLine1 ? data.addressLine1 : '', Validators.required],
            addressLine2: [data && data.addressLine2 ? data.addressLine2 : ''],
            cityName: [data && data.cityName ? data.cityName : '', Validators.required],
            cityId: [data && data.cityId ? data.cityId : ''],
            pinCode: [data && data.pinCode ? data.pinCode : '', this.customValidators.postalCodeValidator],
            accommodationType: [data && data.accommodationType ? data.accommodationType : '', Validators.required],
            mobileNumber: [data && data.mobileNumber ? data.mobileNumber : '', this.customValidators.mobileValidator]
        });

        group.get('cityName').valueChanges
        .pipe(debounceTime(500), 
            switchMap(value =>  { console.log(value, index); return this.getRefCityNames(value, index) } )
        ).subscribe((resp:any) => {
            if(resp.citiesNames && resp.citiesNames.length && resp.status.statusCode == "200"){
                this.cityList = resp.citiesNames;
                this.showRefCitySelection[index] = true;
            }
        }, error => {
            this.alert.error(error.status.statusMessage);
        });

        return group;
    }

    getCityNames(value){
        
        if(!value){
            this.citySelected = false;
            this.showCitySelection = false;
            return empty();
        }
        if(this.citySelected) return empty();

        this.showCitySelection = false;

        if(value && value.length > 2){
            return this.customerService.getCityNames({cityName: value});
        }else{
            return empty();
        }
        
    }

    selectCity(obj){
        this.showCitySelection = false;
        this.citySelected = true;
        if(obj.key){
            this.applicationFormGroup.get('cityId').patchValue(obj.key);
        }
        this.applicationFormGroup.get('cityName').patchValue(obj.value);
    }

    getCityNameDisplayFn(val?: any){
        return (val) => this.displayCityName(val);
    }

    displayCityName(key) {
        let matchedValue:any = '';
        if(this.cityList && this.cityList.length){
            this.cityList.forEach((item) => {
                if(item.key == key)
                    matchedValue = item.value;
            });
        }
        return matchedValue;
    }

    cityShowTooltip(){
        this.toolTipShow = true;
    }

    cityHideTooltip(){
        setTimeout(() => {
            this.showCitySelection = false;
        }, 1000);
        this.toolTipShow = false;
    }

    getRefCityNames(value , index){
        if(!value){
            this.refCitySelected[index] = false;
            this.showRefCitySelection[index] = false;
            return empty();
        }
        if(this.refCitySelected[index]) return empty();

        this.showRefCitySelection[index] = false;

        if(value && value.length > 2){
            return this.customerService.getCityNames({cityName: value});
        }else{
            return empty();
        }
        
    }

    selectRefCity(obj , referenceRow , index){
        this.showRefCitySelection[index] = false;
        this.refCitySelected[index] = true;
        if(obj.key){
            referenceRow.get('cityId').patchValue(obj.key);
        }
        referenceRow.get('cityName').patchValue(obj.value);
    }

}
