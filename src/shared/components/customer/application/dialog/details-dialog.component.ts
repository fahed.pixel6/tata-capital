import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { CustomValidators, FormSubmittedMatcher } from '../../../../services/custom-validators';
import { UserService } from 'shared/services/user-service';

import { Subscription } from 'rxjs';

import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { ApiService } from 'shared/services/api-service';
import { AlertService } from 'shared/services/alert-service';
import { CustomerService } from '../../../../components/customer/services/customer-service';
import { Router } from '@angular/router';

@Component({
    selector: 'details-dialog',
    templateUrl: './details-dialog.component.html',
    styleUrls: ['./details-dialog.component.sass']
})
export class DetailsDialogComponent {


    private saveSubscription: Subscription;
    
    constructor(@Inject(MAT_DIALOG_DATA) public data: any,
                public dialogRef: MatDialogRef<DetailsDialogComponent>,
                public formSubmittedMatcher: FormSubmittedMatcher,
                private fb: FormBuilder,
                public userService: UserService,
                public apiService: ApiService,
                private alert: AlertService,
                private router: Router,
                public customerService: CustomerService,
                private customValidators: CustomValidators) { 

    }


    ngOnInit() {
        this.saveSubscription = this.apiService.currentAction.subscribe(data => {
            if(data.action == 'close'){
                this.dialogRef.close(data.type);
            }
        });
    }
    ngOnDestroy() {
        this.saveSubscription.unsubscribe();
    }
}




