
import { Component, Input, SimpleChanges } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { debounceTime } from 'rxjs/operators';
import { MatDialog } from '@angular/material';

import { Cache } from '../../../../../services/cache';
import { CustomValidators, FormSubmittedMatcher } from '../../../../../services/custom-validators';
import { UserService } from '../../../../../services/user-service';
import { AlertService } from '../../../../../services/alert-service';
import { ConstantsService } from '../../../../../services/constants-service';
import { CustomerService } from '../../../services/customer-service';
import { ApiService } from '../../../../../services/api-service';
import { AdobeService } from 'shared/components/customer/services/adobe-service';


@Component({
    selector: 'app-e-approval-component',
    templateUrl: './e-approval.component.html'
})

export class AppEApprovalDetailsComponent {

    eApprovalFormGroup: FormGroup;
    formSubmitted: boolean;

    loanLimits: any;
    tenures: any;

    showLoader: boolean;
    decisionLoader: boolean;

    tenureSliderStep: any;
    sliderStep: any;

    errorMessage: string;
    showNeedMore: boolean;

    constructor(private fb: FormBuilder,
        public userService: UserService,
        public cache: Cache,
        private alert: AlertService,
        private router: Router,
        public dialog: MatDialog,
        private route: ActivatedRoute,
        public constantsService: ConstantsService,
        public apiService: ApiService,
        private adobeService: AdobeService,
        public customerService: CustomerService,
        public formSubmittedMatcher: FormSubmittedMatcher,
        private customValidators: CustomValidators) {

        this.showLoader = false;
        this.decisionLoader = true;
        this.loanLimits = { nMinLoanAmount: 100000 };
        this.sliderStep = 1000;
        this.tenureSliderStep = 12;
        this.errorMessage = '';
        this.constantsService.getloanTenure();



        this.formSubmitted = false;
        this.createForm();
        this.onChanges();
        this.showNeedMore = false;

    }

    ngOnInit() {
        this.customerService.getLoanDetails('').then((response: any) => {
            this.eApprovalFormGroup.patchValue(response);
            this.loanLimits.nMaxLoanAmount = response.loanAmount;
            if(response.personalInfo && response.personalInfo.name){
                this.cache.user.firstName = response.personalInfo.name ? response.personalInfo.name : this.cache.user.firstName;
            }
            if (response.needMore == true) {
                this.showNeedMore = true;
            } else {
                this.showNeedMore = false;
            }
        }, (error) => {
            this.alert.error(error);
            this.formSubmitted = false;
        });


        setTimeout(() => {
            this.tenures = this.constantsService.constants.loanTenure;
            if (this.tenures && this.tenures.length) {
                this.loanLimits.nMinTenureMonths = this.tenures[0].value;
                this.loanLimits.nMaxTenureMonths = this.tenures[this.tenures.length - 1].value;
            } else {

            }
        }, 500);


    }

    createForm() {

        this.eApprovalFormGroup = this.fb.group({
            loanAmount: [''],
            interestRate: [''],
            emi: [''],
            processingFee: [''],
            tenure: [''],
        });
    }

    submit() {
        if (!this.formSubmitted) {
            this.formSubmitted = true;
            let data = this.eApprovalFormGroup.getRawValue();
            this.adobeService.callSatellite('bl-eapproval-fill-application');

            this.customerService.saveApprovalDetails(data).then((response: any) => {
                if (this.apiService.isDropOff(response)) {
                    this.afterDropoff(response);
                } else {
                    this.router.navigateByUrl('/application/documents');
                }
                this.formSubmitted = false;
            }, (error) => {
                this.formSubmitted = false;
                this.alert.error(error);
            });
        }
    }

    ngOnChanges(changes: SimpleChanges) {

        if (this.eApprovalFormGroup) {
            this.onChanges();
        }
    }
    onChanges() {
        this.eApprovalFormGroup.get('tenure').valueChanges.pipe(debounceTime(500)).subscribe(value => {
            this.setSliderStep(value);
            this.calculate();
        });
        this.eApprovalFormGroup.get('loanAmount').valueChanges.pipe(debounceTime(500)).subscribe(value => {
            this.setSliderStep(value);

            this.calculate();
        });

        this.eApprovalFormGroup.get('loanAmount').setValidators([this.loanValidator.bind(this)]);
        this.eApprovalFormGroup.get('interestRate').setValidators([Validators.required]);
        this.eApprovalFormGroup.get('processingFee').setValidators([Validators.required]);
        this.eApprovalFormGroup.get('tenure').setValidators([this.tenureValidator.bind(this)]);
    }

    loanAmountBlur() {
        let value = this.eApprovalFormGroup.get('loanAmount').value;

        if (value < this.loanLimits.nMinLoanAmount) {
            this.eApprovalFormGroup.get('loanAmount').patchValue(this.loanLimits.nMinLoanAmount);
        } else {
            if (value > this.loanLimits.nMaxLoanAmount) {
                value = this.loanLimits.nMaxLoanAmount;
            }
            value = Math.round(value / 1000) * 1000;
            this.eApprovalFormGroup.get('loanAmount').patchValue(value);
        }
    }
    tenureBlur() {
        let value = this.eApprovalFormGroup.get('tenure').value;

        if (value < this.loanLimits.nMinTenureMonths) {
            this.eApprovalFormGroup.get('tenure').patchValue(this.loanLimits.nMinTenureMonths);
        } else if (value > this.loanLimits.nMaxTenureMonths) {
            this.eApprovalFormGroup.get('tenure').patchValue(this.loanLimits.nMaxTenureMonths);
        } else {
            console.log(value, value / this.tenureSliderStep);
            let partial = value % this.tenureSliderStep;
            if (partial) {
                let selected = 0;
                if (partial <= 6) selected = Math.floor(value / this.tenureSliderStep) * this.tenureSliderStep;
                else selected = Math.ceil(value / this.tenureSliderStep) * this.tenureSliderStep;
                this.eApprovalFormGroup.get('tenure').patchValue(selected);
            }
        }
    }
    setSliderStep(value) {
        if (value > 60000) {
            this.sliderStep = 10000;
        } else {
            this.sliderStep = 1000;
        }
    }
    setTenureSliderStep() {
        if (this.loanLimits.nMaxTenureMonths > 12) {
            this.tenureSliderStep = 12;
        } else {
            this.tenureSliderStep = 6;
        }
    }
    updateLoanAmount(event) {

        this.eApprovalFormGroup.get('loanAmount').patchValue(event.value);
    }
    updateTenure(event) {
        this.eApprovalFormGroup.get('tenure').patchValue(event.value);
    }
    amountLengthlimit(value) {
        return parseInt(value).toString().length;
    }

    calculate() {
        console.log(this.eApprovalFormGroup);
        if (this.eApprovalFormGroup.valid) {
            this.showLoader = true;
            let postdata = this.eApprovalFormGroup.getRawValue();
            delete postdata.emi;
            delete postdata.processingFee;
            this.customerService.getEMIAmount(postdata).then((response: any) => {
                if (response) {
                    this.eApprovalFormGroup.get('emi').patchValue(response.emi);
                }

                this.showLoader = false;
            }, (error) => {
                this.showLoader = false;
                this.alert.error(error);
            });
        }
    }
    loanValidator(controls: FormControl) {
        if (!this.eApprovalFormGroup) return null;
        if (!controls.value) {
            return {
                loanAmount: { required: true }
            }
        } else if (controls.value < this.loanLimits.nMinLoanAmount ||
            controls.value > this.loanLimits.nMaxLoanAmount) {
            return {
                loanAmount: { limit: true }
            }
        }
    }
    tenureValidator(controls: FormControl) {
        console.log(controls);
        if (!this.eApprovalFormGroup) return null;

        let value = controls.value;
        if (!value) {
            return {
                tenure: { required: true }
            }
        } else if (!(value == 12 || value == 24 || value == 36)) {
            return {
                tenure: { limit: true }
            }
        } else {
            let partial = value % this.tenureSliderStep;
            if (partial) {
                return {
                    tenure: { step: true }
                }
            }
        }
        //return null; 
    }
    setTenures() {
        this.tenures = new Array();
        let selectedTenureRemains = false;
        if (this.eApprovalFormGroup.get('loanAmount').value <= 60000) {
            for (let i = 1, val = 6; val <= this.loanLimits.nMaxTenureMonths; i++ , val = i * 6) {

                if (val >= this.loanLimits.nMinTenureMonths && val <= this.loanLimits.nMaxTenureMonths) {
                    this.tenures.push(val);
                    if (this.eApprovalFormGroup.get('tenure').value == val) selectedTenureRemains = true;
                }
            }
        } else {
            for (let i = 1, val = 12; val <= this.loanLimits.nMaxTenureMonths; i++ , val = i * 12) {
                if (val >= this.loanLimits.nMinTenureMonths && val <= this.loanLimits.nMaxTenureMonths) {
                    this.tenures.push(val);
                    if (this.eApprovalFormGroup.get('tenure').value == val) selectedTenureRemains = true;
                }
            }
        }
        this.setTenureSliderStep();

        if (!selectedTenureRemains) {
            this.eApprovalFormGroup.get('tenure').reset();
        }
    }
    afterDropoff(res) {
        this.customerService.dropOffError = res.status.statusMessage;
        this.customerService.dropOffErrorReason = res.status.dropOffPoint;
        this.customerService.dropOffStatusCode = res.status.statusCode;
        this.router.navigateByUrl('/application/thanks');
    }
    openSoftApprovalDialog() {
    }

    exit() {
        this.router.navigateByUrl('/home')
    }
    goBack() {
        if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
            this.router.navigateByUrl('/application/program');
        }
    }

    goToNeedMore() {
        if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
            this.router.navigate(['/application/program'], { queryParams: { needMore: true } });
        }else{
            this.router.navigate(['/application/details/program'], { queryParams: { needMore: true } });
        }
    }
}
