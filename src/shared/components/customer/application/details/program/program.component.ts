
import { Component, Input, ɵConsole } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';


import { Cache } from '../../../../../services/cache';
import { CustomValidators, FormSubmittedMatcher, ConfirmValidParentMatcher } from '../../../../../services/custom-validators';
import { UserService } from '../../../../../services/user-service';
import { ApiService } from '../../../../../services/api-service';
import { AlertService } from '../../../../../services/alert-service';
import { ConstantsService } from '../../../../../services/constants-service';

import { CustomerService } from '../../../../../components/customer/services/customer-service';
import { AdobeService } from 'shared/components/customer/services/adobe-service';


@Component({
  selector: 'app-program-component',
  templateUrl: './program.component.html'
})

export class AppProgramDetailsComponent {

	programDetailsFormGroup: FormGroup;
	formSubmitted: boolean;
	needMore: boolean;
	customerProgramType: any;

    constructor(private fb: FormBuilder,
				public userService: UserService,
				private route: ActivatedRoute,
                public apiService: ApiService,
                public cache: Cache,
                private alert: AlertService,
                public constantsService: ConstantsService,
				private router: Router,
				private adobeService: AdobeService,
                public customerService: CustomerService,
                private customValidators: CustomValidators,
                public parentErrorStateMatcher: ConfirmValidParentMatcher,
                public formSubmittedMatcher: FormSubmittedMatcher) {

		this.formSubmitted = false;
		this.needMore = false;
		this.createForm();
		
		// this.saveSubscription = apiService.currentAction.subscribe(data => {
		// 	console.log(data)
		// 	if(data.action == 'needMore'){ 
		// 		this.needMore = true;
        //     }
		// });
		// console.log(this.needMore)
	}
	

    ngOnInit() {

		this.route.queryParams.subscribe(params => {
			if(params['needMore'] == 'true'){
				this.needMore = true;	
			}else{
				this.needMore = false;
			}
           
        });

		this.onChanges();
		this.customerService.getProgramDetails({needMore: this.needMore}).then((response: any) => {
			if(response){
				this.programDetailsFormGroup.patchValue(response);
				this.customerProgramType = response.customerProgramType;
				this.cache.user.customerProgramType = response.customerProgramType;
				this.cache.user.loanAmountRequired = response.loanAmountRequired;
				this.cache.set('user', this.cache.user);
			}

			if(response.personalInfo && response.personalInfo.name){
                this.cache.user.firstName = response.personalInfo.name ? response.personalInfo.name : this.cache.user.firstName;
            }
			
			
			if(response && response.entityName){
				this.programDetailsFormGroup.get('entityName').patchValue(response.entityName);
				this.programDetailsFormGroup.get('entityName').disable();
			}

			if(this.customerProgramType == 'LTBL'){
				this.programDetailsFormGroup.get('previousYearTurnover').disable();
				this.programDetailsFormGroup.get('last1FYTurover').disable();
				this.programDetailsFormGroup.get('previousYearDeprication').disable();
				this.programDetailsFormGroup.get('partnerDirRem').disable();
				this.programDetailsFormGroup.get('otherIncome').disable();
				this.programDetailsFormGroup.get('partnerDirInterest').disable();
			}else if(this.customerProgramType == 'ABB'){
				this.programDetailsFormGroup.get('previousYearPAT').disable();
				this.programDetailsFormGroup.get('previousYearDeprication').disable();
				this.programDetailsFormGroup.get('partnerDirRem').disable();
				this.programDetailsFormGroup.get('partnerDirInterest').disable();
			}else if(this.customerProgramType == 'VI'){
				this.programDetailsFormGroup.get('last1FYTurover').disable();
			}else if(this.customerProgramType == 'GST'){
				this.programDetailsFormGroup.get('previousYearPAT').disable();
				this.programDetailsFormGroup.get('previousYearDeprication').disable();
				this.programDetailsFormGroup.get('partnerDirInterest').disable();
				this.programDetailsFormGroup.get('partnerDirRem').disable();
			}
			this.formSubmitted = false;
		}, (error) => {
			this.alert.error(error);
			this.formSubmitted = false;
		});
	}
	
	createForm(){
        this.programDetailsFormGroup = this.fb.group({
			loanAmountRequired: ['', [Validators.required,Validators.min(200000),Validators.max(7500000)]],
            entityName: ['', Validators.required],
            previousYearPAT: ['', this.fyPatValidation.bind(this)],
            otherIncome: ['', Validators.required],
            emiPaid: ['', Validators.required],

            previousYearTurnover: ['', this.turnoverValidation.bind(this)],
            last1FYTurover: ['', [Validators.required, Validators.min(100000)]],
            previousYearDeprication: [''],
            partnerDirInterest: ['', Validators.required],
            partnerDirRem: ['', Validators.required],
		});

		this.programDetailsFormGroup.get('previousYearPAT').valueChanges.subscribe((value)=>{
			this.programDetailsFormGroup.get('previousYearPAT').markAsTouched();
           	this.programDetailsFormGroup.get('previousYearTurnover').markAsTouched();
        });
		
    }


    onChanges(){
    
	}
	
  	submit(){                
		let data = this.programDetailsFormGroup.getRawValue();
		
		if(this.customerProgramType == 'LTBL'){
			delete data.previousYearTurnover;
			delete data.last1FYTurover;
			delete data.previousYearDeprication;
			delete data.partnerDirRem;
			delete data.partnerDirInterest;
			delete data.otherIncome;
		}else if(this.customerProgramType == 'ABB'){
			delete data.previousYearPAT;
			delete data.previousYearDeprication;
			delete data.partnerDirRem;
			delete data.partnerDirInterest;
		}else if(this.customerProgramType == 'VI'){
			delete data.last1FYTurover;
		}else if(this.customerProgramType == 'GST'){
			delete data.previousYearPAT;
			delete data.previousYearDeprication;
			delete data.partnerDirInterest;
			delete data.partnerDirRem;
		}


		if(this.programDetailsFormGroup.valid){
			this.formSubmitted = true;

			this.adobeService.callSatellite('bl-program-detail-continue');
			this.customerService.saveProgramDetails(data).then((response: any) => {
				this.formSubmitted = false;
				if(response.status.statusCode == 200){
					this.router.navigateByUrl('/application/details/e-approval');
				}else {
					this.afterDropoff(response);
				}
				
			}, (error) => {
				this.alert.error(error);
				this.formSubmitted = false;
			});
		}else{
            if(!this.programDetailsFormGroup.valid){
                this.alert.error('Please fill all the required fields');
			}
			this.formSubmitted = false;
        }
	  }

	afterDropoff(res){
		this.customerService.dropOffError = res.status.statusMessage;
        this.customerService.dropOffErrorReason = res.status.dropOffPoint;
        this.customerService.dropOffStatusCode = res.status.statusCode;
        this.router.navigateByUrl('/application/thanks');
    }
	  
	  
	fyPatValidation(control: FormControl){

		if (!control || !control.parent) return null;
        let group = control.parent;
        if(group){
			
			let lastFYPAT = control.value.toString().replace(/\D/g, '');
			
            if(!lastFYPAT){
                return { pat: { required: true }};
            }
            if(lastFYPAT < 100000){
                return { pat: { min: true }};
			}
            
			let lastFYTurnover = group.get('previousYearTurnover').value.toString().replace(/\D/g, '');
            if(lastFYTurnover){
                if(Number(lastFYPAT) > Number(lastFYTurnover)){
                    return { pat: { more: true }};

                }
                
            }
        }
      
        return null;
	}
	
	turnoverValidation(control: FormControl){
        if (!control || !control.parent) return null;

        let group = control.parent;
        if(group){
            
            
			let previousYearTurnover = control.value.toString().replace(/\D/g, '');
			if(!previousYearTurnover){
				return { turnover: { required: true }};
			}
			if(previousYearTurnover < 100000){
				return { turnover: { min: true }};
			}
			let previousYearPAT = group.get('previousYearPAT').value.toString().replace(/\D/g, '');
			if(previousYearPAT){
				if(Number(previousYearTurnover) < Number(previousYearPAT)){
					return { turnover: { more: true }};
				}
				
			}
        }
      
        return null;
    }
}
