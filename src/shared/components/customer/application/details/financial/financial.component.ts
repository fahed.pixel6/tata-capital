
import { Component, Input, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { debounceTime } from 'rxjs/operators';
import { MatDialog, MatTabChangeEvent } from '@angular/material';
import { APP_CONFIG, AppConfig } from '../../../../../config/app.config';
import { IAppConfig } from '../../../../../config/iapp.config';

import { Cache } from '../../../../../services/cache';
import { CustomValidators, FormSubmittedMatcher, ConfirmValidParentMatcher } from '../../../../../services/custom-validators';
import { UserService } from '../../../../../services/user-service';
import { ApiService } from '../../../../../services/api-service';
import { AlertService } from '../../../../../services/alert-service';
import { ConstantsService } from '../../../../../services/constants-service';
import { DocumentsService } from '../../../../../services/documents-service';

import { CustomerService } from '../../../../../components/customer/services/customer-service';

import { DetailsDialogComponent } from '../../dialog/details-dialog.component';
import { AdobeService } from 'shared/components/customer/services/adobe-service';


@Component({
  selector: 'app-financial-component',
  templateUrl: './financial.component.html'
})

export class AppFinancialDetailsComponent {

	financialFormGroup: FormGroup;
    formSubmitted: boolean;
    appConfig: IAppConfig;
    _window: any;

    gstSuccess: any;
    itrSuccess: any;
    bankSuccess: any;
    bankFailed: any;
    isEmailSent: any;

    enableContinue: boolean;

    isCustomerJourney: boolean;
    allowedFiles: any[];
    uploadSuccess: any;
    stateLoader: any[];

    constructor(private fb: FormBuilder,
                @Inject(APP_CONFIG) appConfig: IAppConfig,
                public userService: UserService,
                public apiService: ApiService,
                public cache: Cache,
                private alert: AlertService,
                public constantsService: ConstantsService,
                private router: Router,
                private route: ActivatedRoute,
                public dialog: MatDialog,
                public customerService: CustomerService,
                public docService: DocumentsService,
                private adobeService: AdobeService,
                private customValidators: CustomValidators,
                public parentErrorStateMatcher: ConfirmValidParentMatcher,
                public formSubmittedMatcher: FormSubmittedMatcher) {

        this.appConfig = appConfig;
        this.formSubmitted = false;
        this.createForm();
        this.bankSuccess = false;
        this.bankFailed = false;
        this.itrSuccess = false;
        this.gstSuccess = false;
        this.enableContinue = false;
        this.isEmailSent = false;

        this.uploadSuccess = [];
        this.stateLoader = new Array();
        this.allowedFiles = ['pdf', 'png', 'jpeg', 'jpg'];

        this.route.queryParams.subscribe(params => {
            if(params && params.resp){
                let perfiosString: any = atob(params.resp);
                let perfiosData: any = this.parseQuery(perfiosString);        
console.log(perfiosData);
                if(perfiosData && perfiosData.statusCode == '200'){
                    
                    this.adobeService.callSatellite('bl-bank-statement-perfios-success');
                    if(this.userService.isDsaJourney()){
                        this.router.navigate(['/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
                    }else if(this.userService.isCcJourney()){
                        this.router.navigate(['/application/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
                    }else{
                        this.bankFailed = false;
                        this.bankSuccess = true;
                    }

                }else if(perfiosData && perfiosData.statusCode == '734'){
console.log(this.userService.isDsaJourney());
                    if(this.userService.isDsaJourney()){
                        this.router.navigate(['/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
                    }else if(this.userService.isCcJourney()){
                        this.router.navigate(['/application/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
                    }else{
                        this.bankSuccess = false;
                        this.bankFailed = true;
                    }

                }else{

                    if(this.userService.isDsaJourney()){
                        this.router.navigate(['/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
                    }else if(this.userService.isCcJourney()){
                        this.router.navigate(['/application/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
                    }else {
                        this.bankSuccess = false;
                        this.bankFailed = false;
                    }

                }
            }
        });

        this.isCustomerJourney = this.userService.isCustomerJourney();


    }

    parseQuery(search) {

        var args = search.split('&');
        var argsParsed = {};
        var i, arg, kvp, key, value;
        for (i=0; i < args.length; i++) {
            arg = args[i];
            if (-1 === arg.indexOf('=')) {

                argsParsed[decodeURIComponent(arg).trim()] = true;
            }else {

                kvp = arg.split('=');
                key = decodeURIComponent(kvp[0]).trim();
                value = decodeURIComponent(kvp[1]).trim();
                argsParsed[key] = value;
            }
        }
        return argsParsed;
    }

    ngOnInit() {
        this.onChanges();
        this.customerService.getFinancialDetails().then((response: any) => {
            this.financialFormGroup.patchValue(response);
            if(response.personalInfo && response.personalInfo.name){
                this.cache.user.firstName = response.personalInfo.name ? response.personalInfo.name : this.cache.user.firstName;
            }
            if(response.gst == true || response.itr == true || response.perfios == true){
                this.enableContinue = true;
            }
            if(response.bankStatementUploaded){
                this.bankSuccess = true;
            }
            if(response.gstUploaded){
                this.gstSuccess = true;
            }
            if(response.itrUploaded){
                this.itrSuccess = true;
            }
		}, (error) => {
			this.alert.error(error);
			this.formSubmitted = false;
		});

    }
	submit(){
        this.adobeService.callSatellite('bl-financial-detail-continue');
        this.customerService.saveFinancialDetails().then((response: any) => {
			this.router.navigateByUrl('/application/details/program');
		}, (error) => {
			this.alert.error(error);
			this.formSubmitted = false;
		});
				
	}
    onChanges(){
    
    }

    createForm(){
  
        this.financialFormGroup = this.fb.group({
            
        });
    }
    openDetailsDialog(action) {
        
        let dialogRef = this.dialog.open(DetailsDialogComponent, {
            width: '600px',
            data: { action: action}
        });
        dialogRef.afterClosed().subscribe(result => {
console.log(result);
            if(result == 'gst') {
                this.gstSuccess = true;
            }else if(result == 'itr'){
                this.itrSuccess = true;
            }
        });
    }

    goToPerfios(){
        this.adobeService.callSatellite('bl-bank-statement-perfios-click');
        this.userService.setRedirectionAllowed(true);
        window.location.href = this.appConfig.hostCallback + this.appConfig.endpoints.perfiosRedirect +'?customerId='+this.cache.user.customerId;
    }

    goBack(){
        if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
            this.router.navigateByUrl('/eligibility');
        }
    }
    
    exit(){
		this.router.navigateByUrl('/home')
	}

    skipThisStep(){
        this.adobeService.callSatellite('skip-financial-details');
        if(this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
            this.router.navigateByUrl('/application/program'); 
        } else{
            this.router.navigateByUrl('/application/details/program'); 
        }
        
    }


    sendFinancialLink(){
        let data = {serviceType: "FINANCIAL_DETAILS", profile: this.cache.user.profile}
        this.customerService.sendSms(data).then((response: any) => {
console.log(response);
            if(response.status && response.status.statusCode ){
                this.isEmailSent = true;
            }
            
        },(error) => {
            this.isEmailSent = false;
            this.alert.error(error);
        });
    }

    uploadDocuments(files: FileList, elId: string, fieldName: string) {
        let fileName = files[0].name;
        let fileSize = files[0].size;
        

        if (!this.validateFileToUpload(files)) {
            return;
        }
        if(fileSize > 5242880){
            this.alert.error("File size should not be greater than 5MB");
            return false;
        }

        this.stateLoader[elId] = true;

        
        let data = {
            docUploadName: fileName,
            docUploadType: fieldName,
        };
        
        var reader = new FileReader();
        reader.onload = ((theFile) => {
            return (e) => {
                this.docService.uploadFile(e.target.result, data).then( (resp: any) => {
                    if(resp.status.statusCode == 200){
                        this.adobeService.callSatellite('bl-upload-document-success');
                        this.uploadSuccess[elId] = true;
                        this.stateLoader[elId] = false;
                    }else{
                        this.uploadSuccess[elId] = false;
                        this.stateLoader[elId] = false;
                        this.alert.error(resp.status.statusMessage);
                    }
                },  (error) => {
                    this.stateLoader[elId] = false;
                    this.alert.error(error);
                });
            };
        })(files.item(0));
        reader.readAsDataURL(files.item(0));
    }

    validateFileToUpload(files) {
        
        if (!files[0]) {
            return false;
        }
        let fileSize = files[0].size / 1000000;
        let fileName = files[0].name.split('.')[0];
        let extention = files[0].name.split('.').pop().toLowerCase();

        if (fileSize > 10) {
            this.alert.error(files[0].name + '(' + fileSize + ')' + this.apiService.commonStrings.file_size);
            return false;
        } else if (this.allowedFiles.indexOf(extention) == -1) {
            this.alert.error(this.apiService.commonStrings.file_extention);
            return false;
        }
        return true;
    }

    removeUploadedDocument(index){
        this.stateLoader[index] = false;
        this.uploadSuccess[index] = '';
    }
    cancelFailed(){
        this.bankSuccess = false;
        this.bankFailed = false;
    }
}
