import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators, FormSubmittedMatcher } from '../../../../../../services/custom-validators';
import { UserService } from 'shared/services/user-service';

import { empty } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { ApiService } from 'shared/services/api-service';
import { AlertService } from 'shared/services/alert-service';
import { CustomerService } from '../../../../../../components/customer/services/customer-service';
import { Router, ActivatedRoute } from '@angular/router';
import { Cache } from 'shared/services/cache';
import { AdobeService } from 'shared/components/customer/services/adobe-service';


@Component({
	templateUrl: './gst.component.html',
	selector: 'app-gst-component'
})

export class AppGstDetailsComponent {

	gstReportFormGroup: FormGroup;
	formSubmitted: boolean;
	captchaInfo: any = [];
	showSendOtp: boolean;

	public myApplicantDob: INgxMyDpOptions;

	constructor(
			public formSubmittedMatcher: FormSubmittedMatcher,
			private fb: FormBuilder,
			public userService: UserService,
			public apiService: ApiService,
			private alert: AlertService,
			private router: Router,
			private adobeService: AdobeService,
			private route: ActivatedRoute,
			private cache: Cache,
			public customerService: CustomerService,
			private customValidators: CustomValidators) { 

		this.createForm();
		this.formSubmitted = false;
		this.showSendOtp = true;

		this.route.queryParams.subscribe(params => {
			this.cache.loggedIn = false;
            if(params && params.dtls){
                let urlString: any = atob(params.dtls);
				let urlData: any = this.parseQuery(urlString);

				let customerId: any = urlData.cid;
                let contactNumber: any = urlData.contactNumber;
				if(customerId && contactNumber){
					this.cache.user.customerId = customerId;
					this.cache.user.contactNumber = contactNumber;
					this.cache.set('user', this.cache.user);
					this.userService.refreshToken();
					this.apiService.sendAction({action:'hideLogout'});
				}
			}
        });
	}

	parseQuery(search) {

        var args = search.split('&');
        var argsParsed = {};
        var i, arg, kvp, key, value;
        for (i=0; i < args.length; i++) {
            arg = args[i];
            if (-1 === arg.indexOf('=')) {

                argsParsed[decodeURIComponent(arg).trim()] = true;
            }else {

                kvp = arg.split('=');
                key = decodeURIComponent(kvp[0]).trim();
                value = decodeURIComponent(kvp[1]).trim();
                argsParsed[key] = value;
            }
        }
        return argsParsed;
    }


	createForm(){

		this.gstReportFormGroup = this.fb.group({
			gstin: ['', this.customValidators.gstValidator],
			gstUserId: ['', Validators.required],
			otp: ['', Validators.required] 
		});
	}

	ngOnInit() {
		this.onChanges();
	}


	onChanges(){

	}

	gstGenerateOtp(){
		if(this.gstReportFormGroup.get('gstin').valid && this.gstReportFormGroup.get('gstUserId').valid){
			this.userService.gstGenerateOtp({"gstin": this.gstReportFormGroup.get('gstin').value,"username": this.gstReportFormGroup.get('gstUserId').value}).then((response: any) => {
			this.showSendOtp = false;
		}, (error) => {
				this.showSendOtp = false;
				this.alert.error(error);
			});
		}
	}

	submitGstForm(){
		
		let data = this.gstReportFormGroup.getRawValue();
		if(this.gstReportFormGroup.valid){
			this.formSubmitted = true;
			this.adobeService.callSatellite('bl-gst-report-login');
			this.customerService.saveGstDetails(data).then((response: any) => {
				this.adobeService.callSatellite('bl-gst-report-success');
				if(this.userService.isCustomerJourney() || this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
					this.apiService.sendAction({action:'close', data: 'gst'});
				}else{
					this.router.navigate(['/application/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
				}
				this.formSubmitted = false;
			}, (error) => {
				this.alert.error(error);
				this.formSubmitted = false;
			});
		}
	}
	cancelDialog(){
		this.apiService.sendAction({action:'close'});
	}
}
