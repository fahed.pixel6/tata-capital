import { Component, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { CustomValidators, FormSubmittedMatcher } from '../../../../../../services/custom-validators';
import { UserService } from 'shared/services/user-service';

import { empty } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';

import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { ApiService } from 'shared/services/api-service';
import { AlertService } from 'shared/services/alert-service';
import { CustomerService } from '../../../../../../components/customer/services/customer-service';
import { Router, ActivatedRoute } from '@angular/router';
import { Cache } from 'shared/services/cache';

import { DetailsDialogComponent } from '../../../dialog/details-dialog.component';
import { AdobeService } from 'shared/components/customer/services/adobe-service';

@Component({
  templateUrl: './itr.component.html',
  selector: 'app-itr-component'
})

export class AppItrDetailsComponent {

	loginFormGroup: FormGroup;

    bankStatementFormGroup: FormGroup;
	gstReportFormGroup: FormGroup;
	itrStatementFormGroup: FormGroup;
	formSubmitted: boolean;
	captchaInfo: any = [];
	showSendOtp: boolean;

	public myApplicantDob: INgxMyDpOptions;

	constructor(
		public formSubmittedMatcher: FormSubmittedMatcher,
		private fb: FormBuilder,
		public userService: UserService,
		public apiService: ApiService,
		private alert: AlertService,
		private route: ActivatedRoute,
		private router: Router,
		private adobeService: AdobeService,
		private cache: Cache,
		public customerService: CustomerService,
		private customValidators: CustomValidators) { 

			this.createForm();
			this.formSubmitted = false;
			this.showSendOtp = true;

			this.getItrCaptcha();

			this.myApplicantDob = {
				dateFormat: 'dd/mm/yyyy',
				appendSelectorToBody: false,
				disableSince: this.apiService.getCurrentDate(1)
			};

			this.route.queryParams.subscribe(params => {
				this.cache.loggedIn = false;
				if(params && params.dtls){
					let urlString: any = atob(params.dtls);
					let urlData: any = this.parseQuery(urlString);

					let customerId: any = urlData.cid;
					let contactNumber: any = urlData.contactNumber;
					if(customerId && contactNumber){
						this.cache.user.customerId = customerId;
						this.cache.user.contactNumber = contactNumber;
						this.cache.set('user', this.cache.user);
						this.userService.refreshToken();
						this.apiService.sendAction({action:'hideLogout'});					}
				}
			});
	}

	parseQuery(search) {

        var args = search.split('&');
        var argsParsed = {};
        var i, arg, kvp, key, value;
        for (i=0; i < args.length; i++) {
            arg = args[i];
            if (-1 === arg.indexOf('=')) {

                argsParsed[decodeURIComponent(arg).trim()] = true;
            }else {

                kvp = arg.split('=');
                key = decodeURIComponent(kvp[0]).trim();
                value = decodeURIComponent(kvp[1]).trim();
                argsParsed[key] = value;
            }
        }
        return argsParsed;
    }

	createForm(){

		this.itrStatementFormGroup = this.fb.group({
			username: ['', this.customValidators.panValidator],
			password: ['', Validators.required],
			dob: ['', Validators.required],
			year: ['', Validators.required],
			captchaCode: ['', Validators.required] 
		});
	}

	ngOnInit() {
		this.onChanges();
	}


	onChanges(){

	}

	getItrCaptcha(){
		this.captchaInfo.url = '';
		this.userService.getItrCaptcha({"uniqueRequestId": "1079","action": "CAPTCHA"}).then((response: any) => {
			this.captchaInfo.url = response.captchaUrl;
			this.captchaInfo.txnId = response.captchaTxnId;
		}, (error) => {
			this.alert.error(error);
		});
	}

	submitItrForm(){
		
		let data = this.itrStatementFormGroup.getRawValue();
		data.dob = this.apiService.transformDate(data.dob);
		data.action = "RESULT";
		data.captchaTxnId = this.captchaInfo.txnId;
		data.uniqueRequestId = '1079';
		if(this.itrStatementFormGroup.valid){
			this.formSubmitted = true;
			this.adobeService.callSatellite('bl-itr-statement-login');
			this.customerService.getItrDetails(data).then((response: any) => {
				this.adobeService.callSatellite('bl-itr-statement-success');
				if(this.userService.isCustomerJourney() || this.cache.user.profile == 'DSA' || this.cache.user.profile == 'CRE' || this.cache.user.profile == 'SM'){
					this.apiService.sendAction({action:'close', type: 'itr'});
				}else{
					this.router.navigate(['/application/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
				}
				this.formSubmitted = false;
			}, (error) => {
				this.alert.error(error);
				this.formSubmitted = false;
			});
		}else{
			if(!this.itrStatementFormGroup.valid){
				this.alert.error('Please fill all the required fields');
			}
			this.formSubmitted = false;
		}
	}

	cancelDialog(){
		this.apiService.sendAction({action:'close'});
	}

	
}
