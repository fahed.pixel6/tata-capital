
import { Component, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, FormArray } from '@angular/forms';
import { Router } from '@angular/router';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { debounceTime } from 'rxjs/operators';

import { Cache } from '../../../../services/cache';
import { CustomValidators, FormSubmittedMatcher, ConfirmValidParentMatcher } from '../../../../services/custom-validators';
import { UserService } from '../../../../services/user-service';
import { ApiService } from '../../../../services/api-service';
import { AlertService } from '../../../../services/alert-service';
import { ConstantsService } from '../../../../services/constants-service';

import { CustomerService } from '../../../../components/customer/services/customer-service';


@Component({
    selector: 'app-need-more',
    templateUrl: './need-more.component.html'
})

export class AppNeedMoreComponent {

    detailsFormGroup: FormGroup;
    public myApplicantDob: INgxMyDpOptions;
    formSubmitted: boolean;

    constructor(private fb: FormBuilder,
                public userService: UserService,
                public apiService: ApiService,
                public cache: Cache,
                private alert: AlertService,
                public constantsService: ConstantsService,
                private router: Router,
                public customerService: CustomerService,
                private customValidators: CustomValidators,
                public parentErrorStateMatcher: ConfirmValidParentMatcher,
                public formSubmittedMatcher: FormSubmittedMatcher) {

        this.myApplicantDob = {
            dateFormat: 'dd-mm-yyyy',
            appendSelectorToBody: true,
            disableSince: this.apiService.getCurrentDate(1)
        };
        this.createForm();
        this.formSubmitted = false;
        this.customerService.getBusinessDetailsConstants();

    }
        
    createForm(){
        this.detailsFormGroup = this.fb.group({
            applicantName: ['', Validators.required],
            applicantDob: ['', Validators.required],
            applicantPan: ['', this.customValidators.panValidator],
            gender: ['', Validators.required],
            email: ['', this.customValidators.emailValidator]
        });
    }

    ngOnInit() {
        this.customerService.getNeedMore().then((response: any) => {

        }, (error) => {
			this.alert.error(error);
			this.formSubmitted = false;
		});
    }

    submit(){                
        let data = this.detailsFormGroup.getRawValue();
        this.router.navigateByUrl('/application/details/e-approval');
        // if(this.detailsFormGroup.valid){
        //     this.customerService.saveBusinessDetails(data).then((response: any) => {
        //         this.formSubmitted = false;
        //         this.cache.user.panNumber = data.applicantPan;
        //         this.cache.user.city = data.city;
        //         this.cache.set('user', this.cache.user);
        //         this.router.navigateByUrl('/application/details/e-approval');
        //     }, (error) => {
        //         this.alert.error(error);
        //         this.formSubmitted = false;
        //     });
        // }else{
        //     if(!this.detailsFormGroup.valid){
        //         this.alert.error('Please fill all the required feilds');
        //     }
        // }
    }

}
