import { Injectable, Inject } from '@angular/core';

import { APP_CONFIG } from '../../../config/app.config';
import { IAppConfig } from '../../../config/iapp.config';
import { ApiService } from '../../../services/api-service';
import { Cache } from '../../../../shared/services/cache';
import { ConstantsService } from "../../../services/constants-service";

@Injectable()
export class CustomerService {

    appConfig: IAppConfig;

    emiDetails: any;
    getDetailsFromPan: any;

    dropOffError: any;
    dropOffStatusCode: any;
    dropOffErrorReason: any;

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
                private apiService: ApiService,
                public cache: Cache,
                private constantsService: ConstantsService) {
        
        this.appConfig = appConfig;
        this.emiDetails = {};
        this.getDetailsFromPan = {};

        this.dropOffError = "";
        this.dropOffStatusCode="";
        this.dropOffErrorReason = "";
    }
    
    getDetailsByPan(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getDetailsByPan, data, null).then(resp => {
                this.getDetailsFromPan = resp;
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getCityNames(data) {
        return this.apiService.postApiCancellable(this.appConfig.endpoints.getCityNames, data, null);
    }

    getMembershipCAValidate(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getMembershipCAValidate, data, null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getMembershipCOAValidate(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getMembershipCOAValidate, data, null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getDoctorVerificationService(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getDoctorVerificationService, data, null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getBasicDetails() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getBasicDetails, '' ,null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getBusinessDetails() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getBusinessDetails, '' ,null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getProgramDetails(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getCustomerProgramDetails, data ,null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getFinancialDetails() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getFinancialDetails, '' ,null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getApplicationFormDetails() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getApplicationDetails, '' ,null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getDocumentDetails() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getDocumentDetails, '' ,null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getNeedMore() {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getNeedMore, '' ,null).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveBasicDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveBasicDetails, data).then(resp => {
                this.cache.loggedIn = true;
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveBusinessDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveBusinessDetails, data).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveFinancialDetails() {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveFinancialDetails, '').then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveProgramDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveProgramDetails, data).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveApprovalDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveApprovalDetails, data).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveApplicationDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.saveApplicationDetails, data).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getItrDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.getItrData, data).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    saveGstDetails(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.gstSubOtp, data).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    savePan(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.savePan, data).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    verifyPan(pan) {
        let data = { "panNumber": pan };
        return new Promise(function (resolve, reject) {
            this.apiService.postApi(this.appConfig.endpoints.verifyPan, data).then(resp => {
                resolve(resp);
            }, (error) => {
                reject(this.apiService.commonStrings.http_error);
            });
        });
    }

    getDocumentsToUpload() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getDocumentsToUpload, null).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getDocUploadStatus() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getDocUploadStatus, null).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getLoanDetails(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getLoanDetails, data, null).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getEMIAmount(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getEMIAmount, data, null).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getFinalizeCalculation(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getFinalizeCalculation, data, null).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getPLRate(data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(this.appConfig.endpoints.getPLRate, data, null).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    getEMIDetails() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getEMIDetails, null).then((resp) => {
                this.emiDetails = resp;
                resolve(this.emiDetails);
            }, (error) => {
                reject(error);
            });
        });
    }

    sendSms(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.sendSMS, data).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    sendDsaSms(data) {
        return new Promise((resolve, reject) => {
            this.save(this.appConfig.endpoints.sendDsaSMS, data).then((resp) => {
                resolve();
            }, (error) => {
                reject(error);
            });
        });
    }

    getConsent() {
        return new Promise((resolve, reject) => {
            this.get(this.appConfig.endpoints.getConsent, null).then((resp) => {
                resolve(resp);
            }, (error) => {
                reject(error);
            });
        });
    }

    /* Common methods for customer data fetch/save*/
    get(url, data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(url, data, null).then((resp: any) => {
                if (resp && resp.status && resp.status.statusMessage) {
                    if (resp.status.statusMessage.toLowerCase() == 'success' || resp.status.statusCode == '782') {
                        resolve(resp);
                    } else {
                        reject(resp.status.statusMessage ? resp.status.statusMessage : this.apiService.commonStrings.http_error);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(error);
            });
        });
    }

    save(url, data) {
        return new Promise((resolve, reject) => {
            this.apiService.postApi(url, data, null).then((resp: any) => {
                if (resp && resp.status && resp.status.statusMessage) {
                    if (resp.status.statusMessage.toLowerCase() == 'success'  || resp.status.statusCode == '774' || resp.status.statusCode == '701') {
                        resolve(resp);
                    } else if (this.apiService.isDropOff(resp)) {
                        resolve(resp);
                    } else {
                        reject(resp.status.statusMessage ? resp.status.statusMessage : this.apiService.commonStrings.http_error);
                    }
                } else {
                    reject(this.apiService.commonStrings.http_error);
                }
            }, (error) => {
                reject(error);
            });
        });
    }  
    
    getUrlAfterLoginRedirect(stage) {
        if (stage == 'POST_OTP' || stage == 'BASIC_DETAILS' || stage == 'APPLICATION_DETAILS') {
            return [''];//empty cause we want to send user to apps home page
        } else if (stage == 'BUSINESS_DETAILS') {
            return ['/application/', 'business-details'];
        } else if ( stage == 'FINANCIAL_DETAILS') {
            return ['/application/', 'details', 'financial' ];
        } else if (stage == 'ADDITIONAL_BUSINESS_DETAILS') {
            return ['/application/', 'details', 'program'];
        } else if (stage == 'LOAN_ELIGIBILTY_SCREENS') {
            return ['/application/', 'details', 'e-approval'];
        }
        return ['/application/', 'business-details'];
    }

    getBasicDetailsConstants() {
        this.constantsService.getsegmentOfBusiness();
        this.constantsService.getloanTenure();
    }

    getBusinessDetailsConstants() {
        this.constantsService.getIndustryType();
        this.constantsService.getOwnershipCheck();
        this.constantsService.getYearsOfBusiness();
        this.constantsService.getGender();
        this.constantsService.getUniversityName();
        this.constantsService.getStateMedicalCouncil();
    }

    getApplicationFormConstants() {
        this.constantsService.getGender();
        this.constantsService.getMarritalStatus();
        this.constantsService.getYearsInCurrentAddress();
        this.constantsService.getYearsInCurrentCity();
        this.constantsService.getAddressType();
        this.constantsService.getAccommodationType();
    }

    getDocumentsConstants() {
        this.constantsService.getAllDocumentList();
    }

}
