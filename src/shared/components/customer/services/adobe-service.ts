import { Injectable, Inject } from '@angular/core';
import { WINDOW } from 'ngx-window-token';
import { environment } from '../../../../environments/environment';

@Injectable()
export class AdobeService {

    _window: any;

    constructor(@Inject(WINDOW) _window) {
        this._window = _window;
        this._window.digitalData  = this._window.digitalData || [];
        this.debug();
    }

    setPage(name) {
        if (this._window.digitalData && this._window.digitalData.page) {
            this._window.digitalData.page.name = name;
            this.debug();
        }
    }

    setSystem(data) {
        if (this._window.digitalData && this._window.digitalData.system) {
            if (data && data.age) this._window.digitalData.system.age = data.age;
            if (data && data.userID) this._window.digitalData.system.userID = data.userID;
            if (data && data.jocataID) this._window.digitalData.system.jocataID = data.jocataID;
            if (data && data.loanRejectionReason) this._window.digitalData.system.loanRejectionReason = data.loanRejectionReason;
            if (data && data.interestRate) this._window.digitalData.system.intrestRate = data.interestRate;
            if (data && data.emiAmount) this._window.digitalData.system.emiAmount = data.emiAmount;
            if (data && data.loanDisbursementAmount) this._window.digitalData.system.loanDisbursementAmount = data.loanDisbursementAmount;
            if (data && data.vm2decision) this._window.digitalData.system.vm2decision = data.vm2decision;
            if (data && data.verificationMethod) this._window.digitalData.system.verificationMethod = data.verificationMethod;
            if (data && data.webtopId) this._window.digitalData.system.webtopId = data.webtopId;

            this.debug();
        }
    }

    setProduct(name, code) {
        if (this._window.digitalData && this._window.digitalData.product) {
            this._window.digitalData.product.productName = name;
            this._window.digitalData.product.variantCode = code;
            this.debug();
        }
    }

    setError(code, description) {
        if (this._window.digitalData && this._window.digitalData.error) {
            this._window.digitalData.error.errorCode = code;
            this._window.digitalData.error.discription = description;
            this._window.digitalData.system.errorMessage = description;

            this.callSatellite("all-error");
            this.debug();
        }
    }

    setInteraction(data) {
        if (this._window.digitalData && this._window.digitalData.interaction) {
            Object.keys(data).forEach((key) =>  {
                this._window.digitalData.interaction[key] = data[key];
            });
            this.debug();
        }
    }

    callSatelliteForOtp(id, mobileNumer) {
        if (environment.production) {
            if (this._window._satellite) {
                console.log('calling Adobe _satellite.track', id);
                this._window._satellite.track(id, mobileNumer);
            }
        }
    }

    callSatellite(id) {
        if (environment.production) {
            if (this._window.digitalData && this._window._satellite) {
                console.log('calling Adobe _satellite.track', id);
                this._window._satellite.track(id);
            }
        }
    }

    debug() {
        if (!environment.production) console.log(this._window.digitalData);
    }

}
