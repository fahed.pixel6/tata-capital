import { Component, Input } from '@angular/core';

/**
 * The component displayed in the confirmation modal opened by the ConfirmService.
 */
@Component({
    selector: 'app-loader',
    template: `<div *ngIf="type != 3" class="loading-wrap" [class.fullscreen]="type == 2">
                    <div class="loading-icon" [class.small]="type == 0">
                        <img alt="Loading..." src="assets/blue_loader.gif"/>
                    </div>
                </div>
                <div *ngIf="type == 3" class="loading-wrap fullscreen">
                    <div class="loading-icon">
                        <div><img alt="Loading..." src="assets/blue_loader.gif"/></div>
                        <p>Your application is being processed. Please don’t reload the screen or press back button</p>
                    </div>
                </div>`
})

export class LoaderComponent {
    @Input() type: any;

    constructor() {
        this.type = 1;
    }
}
