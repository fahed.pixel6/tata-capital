export class User
{
    appId: string;
    customerId: string;
    employeeId: string;
    cityId: string;
    cityName: string;
    panNumber: string;
    applicantName: string;
    stage: string;
    loanAmountRequired: string;
    customerProgramType: string;
    segmentBusiness: string;
    dateOfIncorporation: string;
    contactNumber: string;
    authorization: string;

    profile: string;
    login: string;
    redirect: string;

    panConstitutionsType: string;

    
    constructor(jsonObj){
        this.appId = jsonObj && jsonObj.appId ? jsonObj.appId : '';
        this.customerId = jsonObj && jsonObj.customerId ? jsonObj.customerId : '';
        this.employeeId = jsonObj && jsonObj.employeeId ? jsonObj.employeeId : '';
        this.cityId = jsonObj && jsonObj.cityId ? jsonObj.cityId : '';
        this.cityName = jsonObj && jsonObj.cityName ? jsonObj.cityName : '';
        this.panNumber = jsonObj && jsonObj.panNumber ? jsonObj.panNumber : '';
        this.applicantName = jsonObj && jsonObj.applicantName ? jsonObj.applicantName : '';
        this.stage = jsonObj && jsonObj.stage ? jsonObj.stage : '';
        this.loanAmountRequired = jsonObj && jsonObj.loanAmountRequired ? jsonObj.loanAmountRequired : '';
        this.customerProgramType = jsonObj && jsonObj.customerProgramType ? jsonObj.customerProgramType : '';
        this.segmentBusiness = jsonObj && jsonObj.segmentBusiness ? jsonObj.segmentBusiness : '';
        this.dateOfIncorporation = jsonObj && jsonObj.dateOfIncorporation ? jsonObj.dateOfIncorporation : '';
        this.contactNumber = jsonObj && jsonObj.contactNumber ? jsonObj.contactNumber : '';
        this.authorization = jsonObj && jsonObj.authorization ? jsonObj.authorization : '';

        this.panConstitutionsType = jsonObj && jsonObj.panConstitutionsType ? jsonObj.panConstitutionsType : '';

        this.profile = jsonObj && jsonObj.profile ? jsonObj.profile : '';
        this.login = jsonObj && jsonObj.login ? jsonObj.login : '';
        this.redirect = jsonObj && jsonObj.redirect ? jsonObj.redirect : '';
     
        
	}
}
