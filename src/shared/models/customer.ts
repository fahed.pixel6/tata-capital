export class ApiStatus {
    
    statusCode: string;
    statusMessage: string;

    constructor(jsonObj) {
        if (jsonObj) {
            this.statusCode = jsonObj.statusCode;
            this.statusMessage = jsonObj.statusMessage;
        } else {
            this.statusCode = '500';
            this.statusMessage = 'Something Went wrong...';
        }
    }
}
