export class Address {

    address1: string;
    address2: string;
    postalCode: string;
    state: string;
    city: string;
    stdCode: string;
    landlineNo: string;
    mobileNo: string;
    ownership: string;
    ownedBy: string;
    isAddrProofAvailable: string;

    constructor(jsonObj) {
	    if (jsonObj) {
	        this.address1 = jsonObj.address1;
	        this.address2 = jsonObj.address2;
            this.postalCode = jsonObj.postalCode;
	        this.state = jsonObj.state;
	        this.city = jsonObj.city;
	        this.stdCode = jsonObj.stdCode;
            this.landlineNo = jsonObj.landlineNo;
            this.mobileNo = jsonObj.mobileNo;
            this.ownership = jsonObj.ownership;
            this.ownedBy = jsonObj.ownedBy;
            this.isAddrProofAvailable = jsonObj.isAddrProofAvailable;
	    } else {
	        this.address1 = '';
	        this.address2 = '';
            this.postalCode = '';
	        this.state = '';
	        this.city = '';
	        this.stdCode = '';
            this.landlineNo = '';
            this.mobileNo = '';
            this.ownership = '';
            this.ownedBy = '';
            this.isAddrProofAvailable = '';
	    }
	}
}


export class DateFormater {

    date: any = {
        "year": '',
        "month": '',
        "day": ''
    };

    constructor(dateString) {
        if (dateString && typeof dateString  == 'string') {
            this.format(dateString);
        } else if(dateString && typeof dateString == 'object') {
            this.date.year = dateString.year;
            this.date.month = dateString.month;
            this.date.day = dateString.day;
        }
    }

    format(date) {
        if (date && typeof date == 'string') {
            let dateArray: any = date.split('/');
            let dateObj: any;
            if (dateArray[2] && dateArray[1] && dateArray[0]) {
                dateObj = new Date(dateArray[2], dateArray[1]-1, dateArray[0]); 
            } else {
                let dateArray: any = date.split(' ');
                dateArray[1] = new Date(dateArray[1]+'-1-01').getMonth()+1;
                if (dateArray[2] && dateArray[1] && dateArray[0]) {
                    dateObj = new Date(dateArray[2], dateArray[1]-1, dateArray[0]);
                }
            }

            if (dateObj) {
                this.date.year = dateObj.getFullYear();
                this.date.month = dateObj.getMonth()+1;
                this.date.day = dateObj.getDate();
                return true;
            } else {
                return false;
            }
        }
        return false;
    }
}

export class DMYDateFormater {
    
    year: string;
    month: string;
    date: string;
    day: string;

    constructor(dateString) {
        if (dateString && typeof dateString  == 'string') {
            this.format(dateString);
        } else if (dateString && typeof dateString == 'object') {
            if (dateString.date) {
                this.year = dateString.date.year ? dateString.date.year : '';
                this.month = dateString.date.month ? dateString.date.month : '';
                this.date = dateString.date.date ? dateString.date.date : '';
            }
        }
    }

    format(date) {
        if (date && typeof date == 'string') {
            let dateArray: any = date.split('/');
            let dateObj: any;
            if (dateArray[2] && dateArray[1] && dateArray[0]){
                dateObj = new Date(dateArray[2], dateArray[1]-1, dateArray[0]); 
            } else {
                let dateArray: any = date.split('-');
                if (dateArray[2] && dateArray[1] && dateArray[0]) {
                    dateObj = new Date(dateArray[2], dateArray[1]-1, dateArray[0]); 
                } 
            }

            if (dateObj) {
                this.year = dateObj.getFullYear();
                this.month = dateObj.getMonth()+1;
                this.date = dateObj.getDate(); 
                return true;
            } else {
                return false;
            }
        }
        return false;
    }

}
