import { Component, ViewChild } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';

import { environment } from '../../../environments/environment';

import { Cache } from "../../../shared/services/cache";


@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.sass']
})

export class FooterComponent {
  date : any;
  hideNiki: boolean;
  navigationSubscription: any;

  @ViewChild('nikiButton') nikiButton;

  constructor(public cache: Cache, private router: Router) {
      this.date =  new Date();
      this.hideNiki = true;

      this.navigationSubscription = this.router.events.subscribe( (event) => {
        if(event instanceof NavigationEnd){
            if(event.urlAfterRedirects.indexOf('/application/information/requirements') == -1 && 
                event.urlAfterRedirects.indexOf('/application/information/detail') == -1 && 
                event.urlAfterRedirects.indexOf('/application/approval/eligibility') == -1 && 
                event.urlAfterRedirects.indexOf('/application/approval/perfios') == -1){
              this.hideNiki = true;
            }else{
              if(environment.envName == 'production'){
                this.hideNiki = true;
              }else{
                this.hideNiki = false;
              }
            }
        }
      });
  }
  ngOnDestroy() {
    this.navigationSubscription.unsubscribe();
  }

}
