import { Component, Inject} from '@angular/core';
import { Router, ActivatedRoute, RouterModule, Params} from '@angular/router';

import { APP_CONFIG, AppConfig } from '../../../shared/config/app.config';
import { IAppConfig } from '../../../shared/config/iapp.config';
import { UserService } from '../../../shared/services/user-service';
import { Cache } from "../../../shared/services/cache";
import { ApiService } from "../../../shared/services/api-service";
import { Subscription } from 'rxjs';


@Component({
selector: 'app-header',
templateUrl: './header.component.html',
styleUrls: ['./header.component.sass']
})

export class HeaderComponent {
    appConfig: any;
    faqPageFlag: boolean = false;
    showLogoutBtn: boolean;
    private saveSubscription: Subscription;

    constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
                private router: Router,
                public cache: Cache,
                public userService: UserService,
                private apiService: ApiService,
                private activatedRoute: ActivatedRoute) {
        this.appConfig = appConfig;
        this.showLogoutBtn = true;
	}

    ngOnInit() {
        // this.router.events.subscribe((url:any) => {
        //     if(this.router.url =='/faq'){
        //         this.faqPageFlag = true;
        //     }
        // });
        this.saveSubscription = this.apiService.currentAction.subscribe(data => {
            if(data.action == 'hideLogout'){
                this.showLogoutBtn = false;
            }
        });
    }
    logout(){
        let home = this.userService.getHomeUrl();
        this.userService.logout();
        this.apiService.sendAction({action:'step', step: 3});
        this.router.navigate(home);
    }
    goHome(){
        this.router.navigate(this.userService.getHomeUrl());
    }
}
