import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';

import { AppCustomerRoutingModule } from './app-customer-routing.module';
import { SharedModule } from '../../../shared/shared.module';
import { AppMaterialModule } from '../../../shared/material.module';

import { ApplicationComponent } from './application.component';
import { AppCustomerThanksComponent } from './thanks/thanks.component';
import { AppDetailsComponent } from './details/details.component';


@NgModule({
  imports: [
    SharedModule,
    AppMaterialModule,
    TranslateModule.forChild(),
    AppCustomerRoutingModule,
  ],
  exports: [
		
  ],
  declarations: [
        ApplicationComponent,
        AppCustomerThanksComponent,
        AppDetailsComponent
    ],
  providers: [
  ],
  entryComponents: [],
})
export class CustomerModule {
  constructor() {
  }
}
