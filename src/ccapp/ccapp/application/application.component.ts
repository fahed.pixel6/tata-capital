import { Component, Inject, ViewChild, OnInit, HostListener } from '@angular/core';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

import { APP_CONFIG, AppConfig } from '../../../shared/config/app.config';
import { IAppConfig } from '../../../shared/config/iapp.config';
import { DocumentsService } from '../../../shared/services/documents-service';
import { UserService } from '../../../shared/services/user-service';
import { AlertService } from '../../../shared/services/alert-service';
import { Cache } from '../../../shared/services/cache';

@Component({
  templateUrl: './application.component.html'
})
export class ApplicationComponent{

  showProgress: boolean;
  navigationSubscription: any;
  step: number;

  appConfig: IAppConfig;
  _window: any;

  constructor(@Inject(APP_CONFIG) appConfig: IAppConfig,
                private router: Router,
                public userService: UserService,
                public alert: AlertService,
                private cache: Cache,
                public docService: DocumentsService,
                private route: ActivatedRoute) {

    this.appConfig = appConfig;
    this.showProgress = false;
    this.step = 0;

    if(!this.userService.isAuthenticated()){
      //router.navigate(this.userService.getHomeUrl(), { replaceUrl: true });
    }
    
    this.navigationSubscription = this.router.events.subscribe( (event) => {
      if(event instanceof NavigationEnd){
          if(event.urlAfterRedirects.indexOf('/application/details/financial') !== -1){
            this.step = 1;
            this.showProgress = true;
          }else if(event.urlAfterRedirects.indexOf('/application/details/program') !== -1){
            this.step = 2;
            this.showProgress = true;
          }else if(event.urlAfterRedirects.indexOf('/application/details/e-approval') !== -1){
            this.step = 3;
            this.showProgress = true;
          }else{
            this.showProgress = false;
          }
      }
    });
  }
  ngOnInit(){
    
  }
  ngOnDestroy() {
    this.navigationSubscription.unsubscribe();
  }
}
