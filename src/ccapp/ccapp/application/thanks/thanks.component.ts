import { Component } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { Cache } from '../../../../shared/services/cache';
import { UserService } from '../../../../shared/services/user-service';
import { ApiService } from '../../../../shared/services/api-service';
import { CustomerService } from 'shared/components/customer/services/customer-service';



@Component({
    selector: 'app-thanks',
    templateUrl: './thanks.component.html'
})

export class AppCustomerThanksComponent {

    showSubmitSection: boolean;

    constructor(public userService: UserService,
                public cache: Cache,
                private route: ActivatedRoute,
                public apiService: ApiService,
                public customerService: CustomerService,
                private router: Router) {
        
        this.showSubmitSection = false;

        this.route.queryParams.subscribe(params => {
            if(params && params.message){
                this.customerService.dropOffError = params.message;
                this.customerService.dropOffStatusCode = '200';
            }
        });

    }
    ngOnInit() {

        this.route.queryParams.subscribe(params => {
            this.showSubmitSection = params['showSubmitSuccess'];
        });

        this.userService.logout();
    }
}
