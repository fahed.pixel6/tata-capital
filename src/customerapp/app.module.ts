import { NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxWindowTokenModule } from 'ngx-window-token';
import { UserIdleModule } from 'angular-user-idle';

import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';
import { SharedModule } from '../shared/shared.module';

import { AppRoutingModule } from './app-routing.module';
import { PALoginComponent } from './account/login.component';

import { AlertComponent } from '../shared/directive/alert.directive';
import { FaqComponent } from './faq/faq.component';

import { AppMaterialModule } from '../shared/material.module';

import { APP_CONFIG, AppConfig } from '../shared/config/app.config';
import { UserService } from '../shared/services/user-service';
import { ApiService } from '../shared/services/api-service';
import { AlertService } from "../shared/services/alert-service";
import { Cache } from '../shared/services/cache';
import { ConstantsService } from '../shared/services/constants-service';
import { CustomValidators, FormSubmittedMatcher, ConfirmValidParentMatcher } from '../shared/services/custom-validators';
import { AuthGuardService } from '../shared/services/auth-guard-service';
import { TokenInterceptor } from '../shared/services/token.interceptor';
import { DocumentsService } from '../shared/services/documents-service';

import { CustomerService } from '../shared/components/customer/services/customer-service';
import { AdobeService } from '../shared/components/customer/services/adobe-service';

@NgModule({
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        SharedModule,
        CoreModule,
        NgxWindowTokenModule,
        AppRoutingModule,
        AppMaterialModule,
        UserIdleModule.forRoot({idle: 900, timeout: 10, ping: 10})],
    exports: [
        AppRoutingModule
        
    ],
    declarations: [
        AppComponent,
        PALoginComponent,
        AlertComponent,
        FaqComponent
    ],
    providers: [
        { provide: APP_CONFIG, useValue: AppConfig },
        Cache,
        UserService,
        ApiService,
        AlertService,
        ConstantsService,
        AuthGuardService,
        CustomValidators,
        FormSubmittedMatcher,
        ConfirmValidParentMatcher,
        DocumentsService,
        AdobeService,
        CustomerService,
        {
            provide: HTTP_INTERCEPTORS,
            useClass: TokenInterceptor,
            multi: true
        }
    ],
    bootstrap: [AppComponent],
    entryComponents: [AlertComponent],

})
export class CustomerAppModule {
    constructor() {
    }
}
