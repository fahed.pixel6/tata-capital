import { NgModule, } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { Error404Component } from '../shared/components/error404/error-404.component';
import { AuthGuardService } from '../shared/services/auth-guard-service';

import { PALoginComponent } from './account/login.component';
import { FaqComponent} from './faq/faq.component'

const routes: Routes = [
  
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'faq', component: FaqComponent },
  { path: 'home', component: PALoginComponent},//for PA app
  { path: 'application',
    loadChildren: './customerapp/application/customer.module#CustomerModule',
    data: { preload: true },
    // canLoad: [AuthGuardService],
    // canActivate: [AuthGuardService]
  },
  { path: '**', component: Error404Component }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {useHash: false})
  ],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {
   
}
