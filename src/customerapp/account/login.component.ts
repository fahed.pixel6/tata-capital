import { Component, Inject, ViewChild, ElementRef} from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, NavigationEnd, ActivatedRoute } from '@angular/router';
import { MatTabChangeEvent } from '@angular/material';
import { MatDialog } from '@angular/material';
import { INgxMyDpOptions } from 'ngx-mydatepicker';
import { Subscription } from 'rxjs';
import { empty } from 'rxjs';
import { debounceTime, switchMap } from 'rxjs/operators';


import { environment } from '../../environments/environment';
import { DMYDateFormater, DateFormater } from '../../shared/models/address';

import { Cache } from '../../shared/services/cache';
import { CustomValidators, FormSubmittedMatcher } from '../../shared/services/custom-validators';
import { UserService } from '../../shared/services/user-service';
import { AlertService } from '../../shared/services/alert-service';
import { ApiService } from '../../shared/services/api-service';
import { CustomerService } from '../../shared/components/customer/services/customer-service';
import { ConstantsService } from '../../shared/services/constants-service';
import { DOCUMENT } from '@angular/platform-browser';
import { AdobeService } from 'shared/components/customer/services/adobe-service';


@Component({
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})

export class PALoginComponent {

	loginFormGroup: FormGroup;
    step: number;

    formSubmitted: boolean;
    mobileText: string;
    otpRefNumber: any;
    otpError: any;
    panMessage: any;
    navigationSubscription: any;
    public mydateOfIncorporationOption: INgxMyDpOptions;
    private valueChangesSubscriptions: Subscription[];

    cityList: any;
    data: any;
    filteredCities: any;

    showCitySelection: boolean;
    citySelected: boolean;
    toolTipShow: boolean;

    loginInfo: any = [];
    checkLoginInfo: boolean;
    checkLoginInfoForFalse: boolean;

    showCityServicableMsg: boolean;
    private saveSubscription: Subscription;

    @ViewChild('city') cityElement: ElementRef;
    @ViewChild('mobileNumber') mobileNumberElement: ElementRef;
    @ViewChild('otp') otpElement: ElementRef;

    constructor(@Inject(DOCUMENT) private document: any,
                private fb: FormBuilder,
                public userService: UserService,
                private cache: Cache,
                private alert: AlertService,
                private router: Router,
                private apiService: ApiService,
                private customerService: CustomerService,
                private constantsService: ConstantsService,
                private customValidators: CustomValidators,
                public formSubmittedMatcher: FormSubmittedMatcher,
                private route: ActivatedRoute,
                private adobeService: AdobeService,
                
                public dialog: MatDialog) {

        this.mydateOfIncorporationOption = {
            dateFormat: 'dd/mm/yyyy',
            appendSelectorToBody: true,
            disableSince: this.apiService.getCurrentDate(0)
        };

        this.loginFormGroup = this.fb.group({
            mobileNumber: ['', this.customValidators.mobileValidator],
            user: this.fb.group({
                cityName: ['', Validators.required],
                cityId: [''],
                panNumber: ['',this.customValidators.panValidator],
                panMessage: [''],
                requiredLoanAmount: ['', [Validators.required,Validators.min(200000),Validators.max(7500000)]],
                segmentOfBusiness: ['', Validators.required],
                loanTenure: ['', Validators.required],
                dateOfIncorporation: [''],
                checkAgreement: [true, Validators.requiredTrue]
            }),
            otp: this.fb.group({
                otp1: [''],
                otp2: [''],
                otp3: [''],
                otp4: ['']
            }, {
                validator: this.otpValidation.bind(this)
            })
        });
        
        this.otpError = '';
        this.cache.user.profile = '';
        this.cache.set('user', this.cache.user);
        this.checkLoginInfo = false;
        this.checkLoginInfoForFalse = false;


        this.route.queryParams.subscribe(params => {
            if(params && params.dtls){

                this.cache.user.profile = '';
                let loginString: any = atob(params.dtls);
                let loginData: any = this.parseQuery(loginString);  
                console.log(loginData);
                if(loginData){
                    this.cache.user.login = loginData.login;
                    this.cache.user.contactNumber = loginData.mobileNumber;
                    this.cache.set('user', this.cache.user);
                    this.loginFormGroup.get('mobileNumber').patchValue(loginData.mobileNumber);

                    if(loginData.login == 'true'){
                        this.checkLoginInfo = true;
                    }else{
                        this.checkLoginInfoForFalse = true;
                    }
                }
            }
        });

    }

    parseQuery(search) {

        var args = search.split('&');
        var argsParsed = {};
        var i, arg, kvp, key, value;
        for (i=0; i < args.length; i++) {
            arg = args[i];
            if (-1 === arg.indexOf('=')) {

                argsParsed[decodeURIComponent(arg).trim()] = true;
            }else {

                kvp = arg.split('=');
                key = decodeURIComponent(kvp[0]).trim();
                value = decodeURIComponent(kvp[1]).trim();
                argsParsed[key] = value;
            }
        }
        return argsParsed;
    }
    
    ngOnInit() {

        if(this.checkLoginInfo == true){
            this.verifyOtp();
        }else{
            this.step = 1;
            this.cache.user.login = '';
            this.cache.user.redirect = '';
            this.cache.set('user', this.cache.user);
            setTimeout(()=>{ this.mobileNumberElement.nativeElement.focus();},0);
        }


        this.loginFormGroup.get('otp').valueChanges.subscribe(status => {
            this.otpError = '';
        });
        this.loginFormGroup.get('mobileNumber').valueChanges.subscribe(status => {
            this.otpError = '';
        });

        this.route.queryParams.subscribe(params => {
            if(params['showthirdStep']){
                this.stageRedirect('BASIC_DETAILS');
            }
        });
        this.saveSubscription = this.apiService.currentAction.subscribe(data => {
            if(data.action == 'step'){ 
                if(data.step == 3){
                    this.step = 1;
                }
            }
        });
        this.onChanges();
        
    }
    ngOnDestroy() {
        this.saveSubscription.unsubscribe();
    }

    onChanges(){

        this.loginFormGroup.get('user').get('panNumber').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value => this.checkPanInfo(value) )
            ).subscribe((resp:any) => {
                console.log(resp);
                if(resp.status && resp.status.statusCode == '200'){
                    if(resp.isValidPAN){
                        this.loginFormGroup.get('user').get('panMessage').patchValue(resp.panStatus);
                        this.cache.user.firstName = resp.firstName ? resp.firstName+' '+ resp.lastName : resp.lastName;
                        this.cache.set('user', this.cache.user);
                    }
                }else{
                    this.loginFormGroup.get('user').get('panMessage').reset();
                    this.loginFormGroup.get('user').get('panNumber').setErrors({pan: {'incorrect': true}});
                }
            }, error => {
                this.loginFormGroup.get('user').get('panMessage').reset();
                this.loginFormGroup.get('user').get('panNumber').setErrors({pan: {'incorrect': true}});
            });

        this.loginFormGroup.get('user').get('cityName').valueChanges
            .pipe(debounceTime(500), 
                switchMap(value =>  { console.log(value); return this.getCityNames(value) } )
            ).subscribe((resp:any) => {
                if(resp.citiesNames && resp.citiesNames.length && resp.status.statusCode == "200"){
                    this.cityList = resp.citiesNames;
                    this.showCitySelection = true;
                }
            }, error => {
                this.alert.error(error.status.statusMessage);
            });
    }

    checkPanInfo(value){
        if(!value || !this.loginFormGroup.get('user').get('panNumber').valid){
            return empty();
        }
        
        this.loginFormGroup.get('user').get('panMessage').reset();
        return this.customerService.getDetailsByPan({panNumber: value, isBaiscDetails: true});
    }

    getCityNames(value){
        
        if(!value){
            this.citySelected = false;
            this.showCitySelection = false;
            return empty();
        }
        if(this.citySelected) return empty();

        this.showCitySelection = false;

        if(value && value.length > 2){
            return this.customerService.getCityNames({cityName: value});
        }else{
            return empty();
        }
        
    }

    selectCity(obj){
        this.showCitySelection = false;
        this.citySelected = true;
        if(obj.key){
            this.loginFormGroup.get('user').get('cityId').patchValue(obj.key);
        }
        this.loginFormGroup.get('user').get('cityName').patchValue(obj.value);
    }

    getCityNameDisplayFn(val?: any){
        return (val) => this.displayCityName(val);
    }

    displayCityName(key) {
        let matchedValue:any = '';
        if(this.cityList && this.cityList.length){
            this.cityList.forEach((item) => {
                if(item.key == key)
                    matchedValue = item.value;
            });
        }
        console.log(matchedValue);
        return matchedValue;
    }

    cityShowTooltip(){
        this.toolTipShow = true;
    }

    cityHideTooltip(){
        setTimeout(() => {
            this.showCitySelection = false;
        }, 1000);
        this.toolTipShow = false;
    }

    goBack(){
        this.step = 1;
    }

    getOtp(){

        this.cache.clear('user');
        this.cache.user = {};
        this.cache.set('user', this.cache.user);
        
        if(!this.loginFormGroup.get('mobileNumber').valid || this.formSubmitted) return;
        
        this.loginFormGroup.get('otp').reset();
        this.mobileText = this.loginFormGroup.get('mobileNumber').value.toString().substring(6,10);
        this.formSubmitted = true;

        this.adobeService.callSatelliteForOtp('bl-send-otp',{ mobileNo: window.btoa(this.loginFormGroup.get('mobileNumber').value) });

        this.userService.sendOtp({mobileNumber: this.loginFormGroup.get('mobileNumber').value, deviceOs: "web"}, 'web').then((response: any) => {
            
            this.formSubmitted = false;
            if(response.status && response.status.statusCode == '200'){
                this.otpRefNumber = response.refId;
                this.cache.user.contactNumber = response.mobileNumber;
                this.cache.set('user', this.cache.user);
                this.step = 2;
                setTimeout(()=>{ this.otpElement.nativeElement.focus();},0);
            }else{
                this.otpRefNumber = response.refId;
                this.formSubmitted = false;
                this.otpError = response.status.statusMessage;
            }
            
        }, (error) => {
            if(error.status && error.status.statusMessage){
                this.alert.error(error.status.statusMessage);
            }
            this.formSubmitted = false;
        });
    }

    verifyOtp(){

        if(!this.loginFormGroup.get('otp').valid || this.formSubmitted) {
            return
        };

        if((!this.loginFormGroup.get('mobileNumber').valid || !this.loginFormGroup.get('otp').valid || this.formSubmitted)) { return };

        let postdata = this.loginFormGroup.get('otp').value;
        this.formSubmitted = true;
        let data;
        let redirect;
        if(this.cache.user.login){
            data = {
                mobileNumber: this.loginFormGroup.get('mobileNumber').value,
                login: this.cache.user.login,
                redirect: true
            };
        }else{

            if(this.checkLoginInfoForFalse){
                redirect = true;
            }else{
                redirect = false;
            }
            data = {
                mobileNumber: this.loginFormGroup.get('mobileNumber').value,
                otp: postdata.otp1.toString() + postdata.otp2.toString() + postdata.otp3.toString() + postdata.otp4.toString(), 
                refId: this.otpRefNumber,
                redirect: redirect,
                deviceOS: "Web",
                customerId: 0
            };
        }

        this.userService.login(data, 'mobile').then((response: any) => {

            if(response.status){
                if(response.status.statusCode == '200'){
                    this.adobeService.callSatellite('bl-otp-verify-success');
                    if(response.personalInfo && response.personalInfo.customerId){
                        this.cache.user.customerId = response.personalInfo.customerId;
                    }
                    this.cache.user.stage = response.personalInfo.stage;
                    this.cache.user.firstName = response.personalInfo.name ? response.personalInfo.name : this.cache.user.firstName;
                    this.cache.set('user', this.cache.user);
                    this.stageRedirect(response.personalInfo.stage);
                }else if(response.status.statusCode == '716' || response.status.statusCode == '703'){
                    this.otpError = response.status.statusMessage;
                    this.adobeService.callSatellite('bl-otp-failure');
                }else{
                    this.adobeService.callSatellite('bl-otp-failure');
                    this.afterDropoff(response);
                }
            }
            this.formSubmitted = false;

        }, (error) => {
            this.alert.error(error);
            this.loginFormGroup.get('otp').reset();
            this.formSubmitted = false;
        });
    }

    submitBasicDetails(){
        this.formSubmitted = true;
        let data = this.loginFormGroup.getRawValue().user;

        data.dateOfIncorporation = this.apiService.transformDate(data.dateOfIncorporation);
        console.log(data.dateOfIncorporation);
        delete data.checkAgreement;
        delete data.panMessage;

        if(this.loginFormGroup.get('user').valid){
            this.adobeService.callSatellite('bl-basic-detail-continue');
            this.customerService.saveBasicDetails(data).then((response: any) => {
                if(response.status && response.status.statusCode == 200){
                    this.formSubmitted = false;
                    this.cache.user.applicantName = response.applicantName;
                    this.cache.set('user', this.cache.user);
                    this.router.navigateByUrl('/application/business-details');
                }else{
                    this.afterDropoff(response);
                }
            }, (error) => {
                this.alert.error(error);
                this.formSubmitted = false;
            });
        }
    }
    
    goToStep(step){
        this.step = step;
    }

    otpValidation (group: FormGroup){
        if(!this.loginFormGroup) return null;

        let ret = null;
        if(this.step == 2){
            let otpControl: FormControl = <FormControl> group.controls['otp1'];
            ret = Validators.required(otpControl);
            if(ret) return ret;

            let otpControl1: FormControl = <FormControl> group.controls['otp2'];
            ret = Validators.required(otpControl1);
            if(ret) return ret;
            
            let otpControl2: FormControl = <FormControl> group.controls['otp3'];
            ret = Validators.required(otpControl2);
            if(ret) return ret;
            
            let otpControl3: FormControl = <FormControl> group.controls['otp4'];
            ret = Validators.required(otpControl3);
            console.log(ret);
            if(ret) return ret;

        }
    
        return null;
    }

    afterDropoff(res){
        this.customerService.dropOffError = res.status.statusMessage;
        this.customerService.dropOffErrorReason = res.status.dropOffPoint;
        this.customerService.dropOffStatusCode = res.status.statusCode;
        this.router.navigateByUrl('/application/thanks');
    }

    stageRedirect(stage){
        let url = this.customerService.getUrlAfterLoginRedirect(stage);
        if(stage == 'POST_OTP' || stage == 'BASIC_DETAILS'){
            this.step = 3;
            setTimeout(()=>{ this.cityElement.nativeElement.focus();},0);
            this.callGetBasicDetailsApi();
        }else if(stage == 'APPLICATION_DETAILS'){
            this.router.navigate(['/application/thanks'], { queryParams: { showSubmitSuccess: 'true' } });
        }else{
            console.log(url);  
            this.router.navigate(url); 
        }
    }

    resumeApplication(){
        this.step = 1;
        this.loginFormGroup.get('mobileNumber').reset();
    }


    callGetBasicDetailsApi(){
        this.customerService.getBasicDetailsConstants();
        this.customerService.getBasicDetails().then((response: any) => {
            if(response.personalInfo && response.personalInfo.name){
                this.cache.user.firstName = response.personalInfo.name ? response.personalInfo.name : this.cache.user.firstName;
            }
            this.loginFormGroup.get('user').patchValue(response, {emitEvent: false});
            this.loginFormGroup.get('user').get('panNumber').patchValue(response.panNumber);
            if(response.dateOfIncorporation){
                this.loginFormGroup.get('user').get('dateOfIncorporation').patchValue(new DateFormater(response.dateOfIncorporation));
            }
        }, (error) => {
            this.alert.error(error);
            this.formSubmitted = false;
        });
    }
   
}
