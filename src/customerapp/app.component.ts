import { Component, Inject } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { DOCUMENT } from '@angular/common';

import { AlertService } from '../shared/services/alert-service';
import { PlatformLocation } from '@angular/common'
import { UserService } from '../shared/services/user-service';
import { Cache } from '../shared/services/cache';
import { environment } from '../environments/environment';

import { CustomerService } from '../shared/components/customer/services/customer-service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

	constructor(@Inject(DOCUMENT) private doc: any,
					private alert: AlertService,
					private route: ActivatedRoute,
					private cache: Cache,
					private router: Router,
					private customerService: CustomerService,
					private userService: UserService) {

	}

	ngOnInit() {
		this.setScripts();
		this.userService.setIsCcJourney(false);
		this.userService.setIsDsaJourney(false);
		this.userService.setIsCustomerJourney(true);
	}

  	setScripts(){
console.log(environment.envName);
		if (environment.envName == 'production') {
			const script = this.doc.createElement('script');
			script.src = '//assets.adobedtm.com/launch-EN6d72ead9fffc43a78e5220d4bb8192d4.min.js';
			document.head.appendChild(script);
		}
		
  	}
}



