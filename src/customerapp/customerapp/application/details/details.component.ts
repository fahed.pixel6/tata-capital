import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';

import { Cache } from '../../../../shared/services/cache';
import { CustomValidators } from '../../../../shared/services/custom-validators';
import { UserService } from '../../../../shared/services/user-service';
import { AlertService } from '../../../../shared/services/alert-service';

@Component({
  templateUrl: './details.component.html'
})

export class AppDetailsComponent {

	loginFormGroup: FormGroup;

    constructor( private alert: AlertService,
                private router: Router,
                private route: ActivatedRoute) {


    }
    ngOnInit() {

    }
}
