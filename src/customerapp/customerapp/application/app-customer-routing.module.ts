import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ApplicationComponent } from './application.component';
import { AppCustomerThanksComponent } from './thanks/thanks.component';
import { AppDetailsComponent } from './details/details.component';

import { AppFinancialDetailsComponent } from '../../../shared/components/customer/application/details/financial/financial.component';
import { AppProgramDetailsComponent } from '../../../shared/components/customer/application/details/program/program.component';
import { AppEApprovalDetailsComponent } from '../../../shared/components/customer/application/details/e-approval/e-approval.component';
import { AppBusinessDetailsComponent } from '../../../shared/components/customer/application/business-details/business-details.component';
import { AppNeedMoreComponent } from '../../../shared/components/customer/application/need-more/need-more.component';
import { AppApplicationFormComponent } from '../../../shared/components/customer/application/application-form/application-form.component';
import { AppDocumentsComponent } from '../../../shared/components/customer/application/documents/documents.component';


const customerRoutes: Routes = [

    {path: '', component: ApplicationComponent,
      children: [
        {path: '', redirectTo: 'business-details', pathMatch: 'full'},
        { path: 'business-details', component: AppBusinessDetailsComponent},
        { path: 'application-form', component: AppApplicationFormComponent},
        { path: 'documents', component: AppDocumentsComponent},
        { path: 'details',  component: AppDetailsComponent,
            children: [
                {path: '', redirectTo: 'financial', pathMatch: 'full'},
                {path: 'financial', component: AppFinancialDetailsComponent},
                {path: 'program', component: AppProgramDetailsComponent},
                {path: 'e-approval', component: AppEApprovalDetailsComponent},
            ]
        },
        {path: 'need-more', component: AppNeedMoreComponent},
        {path: 'thanks', component: AppCustomerThanksComponent},
      ]
    }
];
@NgModule({
  imports: [
    RouterModule.forChild(customerRoutes)
  ],
  exports: [
    RouterModule
  ]
})

export class AppCustomerRoutingModule {
}
